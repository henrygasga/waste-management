package school.wastemanagement.base;

import android.content.Context;
import android.media.MediaPlayer;
import android.os.Build;
import android.widget.ImageView;
import android.widget.LinearLayout;

import school.wastemanagement.R;

public class Utils {

    public static void setGrade(Context context, int level, ImageView imgLevel, LinearLayout levelBg) {
        int bg = 0;
        int bgLvl = 0;
        switch (level) {
            case 1:
                bgLvl = R.drawable.iv_lvl1;
                bg = R.drawable.bg1;
                break;
            case 2:
                bgLvl = R.drawable.iv_lvl2;
                bg = R.drawable.bg2;
                break;
            case 3:
                bgLvl = R.drawable.iv_lvl3;
                bg = R.drawable.bg3;
                break;
            case 4:
                bgLvl = R.drawable.iv_lvl4;
                bg = R.drawable.bg4;
                break;
            case 5:
                bgLvl = R.drawable.iv_lvl5;
                bg = R.drawable.bg5;
                break;
            case 6:
                bgLvl = R.drawable.iv_lvl6;
                bg = R.drawable.bg6;
                break;
            case 7:
                bgLvl = R.drawable.iv_lvl7;
                bg = R.drawable.bg7;
                break;
            case 8:
                bgLvl = R.drawable.iv_lvl8;
                bg = R.drawable.bg8;
                break;
            case 9:
                bgLvl = R.drawable.iv_lvl9;
                bg = R.drawable.bg9;
                break;
            case 10:
                bgLvl = R.drawable.iv_lvl10;
                bg = R.drawable.bg1;
                break;
            case 11:
                bgLvl = R.drawable.iv_lvl11;
                bg = R.drawable.bg2;
                break;
            case 12:
                bgLvl = R.drawable.iv_lvl12;
                bg = R.drawable.bg3;
                break;
            case 13:
                bgLvl = R.drawable.iv_lvl13;
                bg = R.drawable.bg4;
                break;
            case 14:
                bgLvl = R.drawable.iv_lvl14;
                bg = R.drawable.bg5;
                break;
            case 15:
                bgLvl = R.drawable.iv_lvl15;
                bg = R.drawable.bg6;
                break;
            case 16:
                bgLvl = R.drawable.iv_lvl16;
                bg = R.drawable.bg7;
                break;
            case 17:
                bgLvl = R.drawable.iv_lvl17;
                bg = R.drawable.bg8;
                break;
            case 18:
                bgLvl = R.drawable.iv_lvl18;
                bg = R.drawable.bg9;
                break;
            case 19:
                bgLvl = R.drawable.iv_lvl19;
                bg = R.drawable.bg1;
                break;
            case 20:
                bgLvl = R.drawable.iv_lvl20;
                bg = R.drawable.bg2;
                break;
            default:
                imgLevel.setImageDrawable(context.getResources().getDrawable(R.drawable.iv_lvl1));
        }
        imgLevel.setImageDrawable(context.getResources().getDrawable(bgLvl));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            levelBg.setBackground(context.getResources().getDrawable(bg));
        }
    }

    public static void soundFail(Context context){
        final MediaPlayer mp2 = MediaPlayer.create(context, R.raw.fail);
        if(mp2.isPlaying())
            mp2.pause();
        else
            mp2.start();
    }

    public static void soundWin(Context context){
        final MediaPlayer mp2 = MediaPlayer.create(context, R.raw.win);
        if(mp2.isPlaying())
            mp2.pause();
        else
            mp2.start();
    }

    public static String getGType(int type) {
        switch (type) {
            case R.id.img_nonbio:
                return Constant.GARBAGE_NON_BIO;
            case R.id.img_bio:
                return Constant.GARBAGE_BIO;
            case R.id.img_recycle:
                return Constant.GARBAGE_REC;
            default:
                return "";
        }
    }
}
