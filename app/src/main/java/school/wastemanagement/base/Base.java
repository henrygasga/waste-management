package school.wastemanagement.base;

import android.content.Context;

public interface Base {
    Context getContext();
}
