package school.wastemanagement.base;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatDialog;
import android.support.v7.widget.AppCompatButton;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import butterknife.ButterKnife;
import school.wastemanagement.R;

public class DialogFactory {
    public static final int OK_DIALOG = 0;
    public static final int YES_OR_NO_DIALOG = 1;

    public static final int BLANK = -1;
    public static final int ERROR = 0;
    public static final int SUCCESS = 1;

    @IntDef({BLANK, ERROR, SUCCESS})
    @Retention(RetentionPolicy.SOURCE)
    private @interface DialogResult {}

    public static ProgressDialog createProgressDialog(Context context, String message) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        return progressDialog;
    }

    public static AppCompatDialog createTriviaDialog(@NonNull Context context, int src, final DialogButtonOnClickListener clickListener) {
        //region Create and show a custom dialog
        final AppCompatDialog dialog = new AppCompatDialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(false);
        View view;

        //region Dialog View
        // Show Colored dialog with one button
        view = LayoutInflater.from(context).inflate(R.layout.dialog_trivia, null);
        AppCompatButton buttonPositive = ButterKnife.findById(view, R.id.button_ok);

        // Use OK text if no positive button text parameter was assigned, else use the one in the parameter.
        buttonPositive.setText("Ok");

        buttonPositive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                clickListener.onPositiveButtonClick();
            }
        });

        //endregion
        // Message
        ((ImageView) ButterKnife.findById(view, R.id.imgTrivia)).setImageDrawable(context.getResources().getDrawable(src));

        //region Sub Message

        dialog.setContentView(view);
        //endregion

        return dialog;
        //endregion
    }

    public static AppCompatDialog createColoredDialog(@NonNull Context context, String message, final DialogButtonOnClickListener clickListener) {
        //region Create and show a custom dialog
        final AppCompatDialog dialog = new AppCompatDialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(false);
        View view;

        //region Dialog View
        // Show Colored dialog with one button
        view = LayoutInflater.from(context).inflate(R.layout.dialog_result, null);
        AppCompatButton buttonPositive = ButterKnife.findById(view, R.id.button_ok);

        // Use OK text if no positive button text parameter was assigned, else use the one in the parameter.
        buttonPositive.setText("Ok");

        buttonPositive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                clickListener.onPositiveButtonClick();
            }
        });

        //endregion
        // Message
        ((TextView) ButterKnife.findById(view, R.id.text_view_dialog_message)).setText(message);

        //region Sub Message

        dialog.setContentView(view);
        //endregion

        return dialog;
        //endregion
    }

    public interface DialogButtonOnClickListener {
        void onPositiveButtonClick();
    }
}
