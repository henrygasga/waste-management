package school.wastemanagement.base;

import android.content.Context;
import android.media.MediaPlayer;

import java.io.IOException;

import school.wastemanagement.R;


public class Dj {
    private static MediaPlayer player;
    private static boolean keepMusicOn;

    public static void iAmIn(Context context){
        if(player == null) {
            player = MediaPlayer.create(context, R.raw.london);
            player.setLooping(true);
            player.setVolume(100, 100);

            try {
                player.prepare();
            }catch (IllegalStateException e){}
            catch (IOException e){}
        }

        if(!player.isPlaying()) {
            player.start();
        }


        keepMusicOn = false;
    }

    public static void KeepMusicOn(){
        keepMusicOn = true;
    }

    public static void iAmLeaving() {
        if(!keepMusicOn){
            player.pause();
        }
    }

    public static void stopMusic() {
        if(player.isPlaying()) {
            player.pause();
        }
    }

    public static boolean isKeepMusicOn() {
        return keepMusicOn;
    }
}
