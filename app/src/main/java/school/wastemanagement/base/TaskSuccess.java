package school.wastemanagement.base;


import android.content.Context;
import android.os.AsyncTask;

public class TaskSuccess extends AsyncTask<Void, Void, Exception> {
    Context mContext;
    public TaskSuccess(Context activity) {
        this.mContext = activity;
    }
    @Override
    protected Exception doInBackground(Void... params) {
        Utils.soundWin(mContext);
        return null;
    }
    @Override
    protected void onPostExecute(Exception result) {

    }
}
