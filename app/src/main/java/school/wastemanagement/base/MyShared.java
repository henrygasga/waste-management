package school.wastemanagement.base;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import java.util.Map;

import school.wastemanagement.model.GarbageModel;

public class MyShared {

    private static final String APP_SETTINGS = "APP_SETTINGS";
    
    private static final String energy = "energy";
    private static final String level1 = "level1";
    private static final String level2 = "level2";
    private static final String level3 = "level3";
    private static final String level4 = "level4";
    private static final String level5 = "level5";
    private static final String level6 = "level6";
    private static final String level7 = "level7";
    private static final String level8 = "level8";
    private static final String level9 = "level9";
    private static final String level10 = "level10";
    private static final String level11 = "level11";
    private static final String level12 = "level12";
    private static final String level13 = "level13";
    private static final String level14 = "level14";
    private static final String level15 = "level15";
    private static final String level16 = "level16";
    private static final String level17 = "level17";
    private static final String level18 = "level18";
    private static final String level19 = "level19";
    private static final String level20 = "level20";

    public MyShared() {
    }
    
    public static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences(APP_SETTINGS, Context.MODE_PRIVATE);
    }

    public static int getEnergy(Context context) {
        return getSharedPreferences(context).getInt(energy, 15);
    }

    public static boolean isLevel1(Context context) {
        return getSharedPreferences(context).getBoolean(level1, false);
    }

    public static boolean isLevel2(Context context) {
        return getSharedPreferences(context).getBoolean(level2, false);
    }

    public static boolean isLevel3(Context context) {
        return getSharedPreferences(context).getBoolean(level3, false);
    }

    public static boolean isLevel4(Context context) {
        return getSharedPreferences(context).getBoolean(level4, false);
    }

    public static boolean isLevel5(Context context) {
        return getSharedPreferences(context).getBoolean(level5, false);
    }

    public static boolean isLevel6(Context context) {
        return getSharedPreferences(context).getBoolean(level6, false);
    }

    public static boolean isLevel7(Context context) {
        return getSharedPreferences(context).getBoolean(level7, false);
    }

    public static boolean isLevel8(Context context) {
        return getSharedPreferences(context).getBoolean(level8, false);
    }

    public static boolean isLevel9(Context context) {
        return getSharedPreferences(context).getBoolean(level9, false);
    }

    public static boolean isLevel10(Context context) {
        return getSharedPreferences(context).getBoolean(level10, false);
    }

    public static boolean isLevel11(Context context) {
        return getSharedPreferences(context).getBoolean(level11, false);
    }

    public static boolean isLevel12(Context context) {
        return getSharedPreferences(context).getBoolean(level12, false);
    }

    public static boolean isLevel13(Context context) {
        return getSharedPreferences(context).getBoolean(level13, false);
    }

    public static boolean isLevel14(Context context) {
        return getSharedPreferences(context).getBoolean(level14, false);
    }

    public static boolean isLevel15(Context context) {
        return getSharedPreferences(context).getBoolean(level15, false);
    }

    public static boolean isLevel16(Context context) {
        return getSharedPreferences(context).getBoolean(level16, false);
    }

    public static boolean isLevel17(Context context) {
        return getSharedPreferences(context).getBoolean(level17, false);
    }

    public static boolean isLevel18(Context context) {
        return getSharedPreferences(context).getBoolean(level18, false);
    }

    public static boolean isLevel19(Context context) {
        return getSharedPreferences(context).getBoolean(level19, false);
    }

    public static boolean isLevel20(Context context) {
        return getSharedPreferences(context).getBoolean(level20, false);
    }

    public static void setEnergy(Context context, int o) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putInt(energy, o);
        editor.apply();
    }

    public static void setLevel1(Context context, boolean o) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(level1, o);
        editor.apply();
    }

    public static void setLevel2(Context context, boolean o) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(level2, o);
        editor.apply();
    }
    public static void setLevel3(Context context, boolean o) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(level3, o);
        editor.apply();
    }

    public static void setLevel4(Context context, boolean o) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(level4, o);
        editor.apply();
    }

    public static void setLevel5(Context context, boolean o) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(level5, o);
        editor.apply();
    }

    public static void setLevel6(Context context, boolean o) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(level6, o);
        editor.apply();
    }

    public static void setLevel7(Context context, boolean o) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(level7, o);
        editor.apply();
    }

    public static void setLevel8(Context context, boolean o) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(level8, o);
        editor.apply();
    }

    public static void setLevel9(Context context, boolean o) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(level9, o);
        editor.apply();
    }

    public static void setLevel10(Context context, boolean o) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(level10, o);
        editor.apply();
    }

    public static void setLevel11(Context context, boolean o) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(level11, o);
        editor.apply();
    }

    public static void setLevel12(Context context, boolean o) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(level12, o);
        editor.apply();
    }

    public static void setLevel13(Context context, boolean o) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(level13, o);
        editor.apply();
    }

    public static void setLevel14(Context context, boolean o) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(level14, o);
        editor.apply();
    }

    public static void setLevel15(Context context, boolean o) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(level15, o);
        editor.apply();
    }

    public static void setLevel16(Context context, boolean o) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(level16, o);
        editor.apply();
    }

    public static void setLevel17(Context context, boolean o) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(level17, o);
        editor.apply();
    }

    public static void setLevel18(Context context, boolean o) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(level18, o);
        editor.apply();
    }

    public static void setLevel19(Context context, boolean o) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(level19, o);
        editor.apply();
    }

    public static void setLevel20(Context context, boolean o) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(level20, o);
        editor.apply();
    }

}
