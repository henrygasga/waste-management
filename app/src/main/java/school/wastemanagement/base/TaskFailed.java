package school.wastemanagement.base;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

public class TaskFailed extends AsyncTask<Void, Void, Exception> {
    Context mContext;
    public TaskFailed(Context activity) {
        this.mContext = activity;
    }
    @Override
    protected Exception doInBackground(Void... params) {
        Utils.soundFail(mContext);
        return null;
    }
    @Override
    protected void onPostExecute(Exception result) {

    }
}
