package school.wastemanagement.base;

import android.view.DragEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

public class MyDragListener implements View.OnDragListener {

    DropListener dropListener;

    public void setDropListener(DropListener dropListener) {
        this.dropListener = dropListener;
    }

    @Override
    public boolean onDrag(View view, DragEvent dragEvent) {
        int action = dragEvent.getAction();
        switch (action){
            case DragEvent.ACTION_DRAG_STARTED:
                break;
            case DragEvent.ACTION_DRAG_ENTERED:
                break;
            case DragEvent.ACTION_DRAG_EXITED:
                break;
            case DragEvent.ACTION_DROP:
                View v = (View) dragEvent.getLocalState();
                ViewGroup owner = (ViewGroup) v.getParent();
                owner.removeView(v);
                if(dropListener != null) dropListener.processDrop(view.getId(), v, owner);
//                    processDrop(view.getId(), v, owner);
                break;
            case DragEvent.ACTION_DRAG_ENDED:
                break;
        }
        return true;
    }

    public interface DropListener {
        void processDrop(int viewId, View v, ViewGroup vg);
    }
}
