package school.wastemanagement.base;

import java.util.Map;

import school.wastemanagement.model.GarbageModel;

public class Singleton {
    private static Singleton instance;
    private boolean stopTime = false;
    private int c1Item = 0;
    private int c2Item = 0;
    private int c3Item = 0;
    private int energy = 15;
    private boolean level1 = false;
    private boolean level2 = false;
    private boolean level3 = false;
    private boolean level4 = false;
    private boolean level5 = false;
    private boolean level6 = false;
    private boolean level7 = false;
    private boolean level8 = false;
    private boolean level9 = false;
    private boolean level10 = false;
    private boolean level11 = false;
    private boolean level12 = false;
    private boolean level13 = false;
    private boolean level14 = false;
    private boolean level15 = false;
    private boolean level16 = false;
    private boolean level17 = false;
    private boolean level18 = false;
    private boolean level19 = false;
    private boolean level20 = false;
    private Map<Integer, GarbageModel> guideGarbage;

    private Singleton() {
    }

    public static Singleton getInstance() {
        if (instance == null){
            //Thread safe. Might be costly operation in some case.
            synchronized (Singleton.class) {
                if (instance == null) {
                    instance = new Singleton();
                }
            }
        }
        return instance;

    }

    public boolean isStopTime() {
        return stopTime;
    }

    public void setStopTime(boolean stopTime) {
        this.stopTime = stopTime;
    }

    public int getC1Item() {
        return c1Item;
    }

    public void setC1Item(int c1Item) {
        this.c1Item = c1Item;
    }

    public int getC2Item() {
        return c2Item;
    }

    public void setC2Item(int c2Item) {
        this.c2Item = c2Item;
    }

    public int getC3Item() {
        return c3Item;
    }

    public void setC3Item(int c3Item) {
        this.c3Item = c3Item;
    }


    public Map<Integer, GarbageModel> getGuideGarbage() {
        return guideGarbage;
    }

    public void setGuideGarbage(Map<Integer, GarbageModel> guideGarbage) {
        this.guideGarbage = guideGarbage;
    }

    public boolean isLevel1() {
        return level1;
    }

    public void setLevel1(boolean level1) {
        this.level1 = level1;
    }

    public boolean isLevel2() {
        return level2;
    }

    public void setLevel2(boolean level2) {
        this.level2 = level2;
    }

    public boolean isLevel3() {
        return level3;
    }

    public void setLevel3(boolean level3) {
        this.level3 = level3;
    }

    public boolean isLevel4() {
        return level4;
    }

    public void setLevel4(boolean level4) {
        this.level4 = level4;
    }

    public boolean isLevel5() {
        return level5;
    }

    public void setLevel5(boolean level5) {
        this.level5 = level5;
    }

    public boolean isLevel6() {
        return level6;
    }

    public void setLevel6(boolean level6) {
        this.level6 = level6;
    }

    public boolean isLevel7() {
        return level7;
    }

    public void setLevel7(boolean level7) {
        this.level7 = level7;
    }

    public boolean isLevel8() {
        return level8;
    }

    public void setLevel8(boolean level8) {
        this.level8 = level8;
    }

    public boolean isLevel9() {
        return level9;
    }

    public void setLevel9(boolean level9) {
        this.level9 = level9;
    }

    public boolean isLevel10() {
        return level10;
    }

    public void setLevel10(boolean level10) {
        this.level10 = level10;
    }

    public boolean isLevel11() {
        return level11;
    }

    public void setLevel11(boolean level11) {
        this.level11 = level11;
    }

    public boolean isLevel12() {
        return level12;
    }

    public void setLevel12(boolean level12) {
        this.level12 = level12;
    }

    public boolean isLevel13() {
        return level13;
    }

    public void setLevel13(boolean level13) {
        this.level13 = level13;
    }

    public boolean isLevel14() {
        return level14;
    }

    public void setLevel14(boolean level14) {
        this.level14 = level14;
    }

    public boolean isLevel15() {
        return level15;
    }

    public void setLevel15(boolean level15) {
        this.level15 = level15;
    }

    public boolean isLevel16() {
        return level16;
    }

    public void setLevel16(boolean level16) {
        this.level16 = level16;
    }

    public boolean isLevel17() {
        return level17;
    }

    public void setLevel17(boolean level17) {
        this.level17 = level17;
    }

    public boolean isLevel18() {
        return level18;
    }

    public void setLevel18(boolean level18) {
        this.level18 = level18;
    }

    public boolean isLevel19() {
        return level19;
    }

    public void setLevel19(boolean level19) {
        this.level19 = level19;
    }

    public boolean isLevel20() {
        return level20;
    }

    public void setLevel20(boolean level20) {
        this.level20 = level20;
    }

    public int getEnergy() {
        return energy;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }
}
