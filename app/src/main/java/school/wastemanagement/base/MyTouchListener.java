package school.wastemanagement.base;

import android.content.ClipData;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

public final class MyTouchListener implements View.OnTouchListener{

    RemoveGrass removeGrass;

    public void setRemoveGrass(RemoveGrass removeGrass) {
        this.removeGrass = removeGrass;
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
            ClipData data = ClipData.newPlainText("","");
            View v;
            if (view instanceof ForegroundImageView) {
                ForegroundImageView fv = (ForegroundImageView) view;
                fv.setForeground(null);
                v = fv;
            } else {
                v = view;
            }

            View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(v);
            view.startDrag(data, shadowBuilder, v, 0);

            return true;
        } else {
            return false;
        }
    }

    public interface RemoveGrass {
        void removeGrass();
    }
}
