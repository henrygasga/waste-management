package school.wastemanagement.base;

import school.wastemanagement.R;

public class Constant {

    //type of garbage
    public final static String GARBAGE_BIO = "Biodegradable";
    public final static String GARBAGE_NON_BIO = "Non-Biodegradable";
    public final static String GARBAGE_REC = "Recyclable";
    public final static String GARBAGE_ALL = "all";


    //levels
    public final static int LEVEL_1 = 1;
    public final static int LEVEL_2 = 2;
    public final static int LEVEL_3 = 3;
    public final static int LEVEL_4 = 4;
    public final static int LEVEL_5 = 5;
    public final static int LEVEL_6 = 6;
    public final static int LEVEL_7 = 7;
    public final static int LEVEL_8 = 8;
    public final static int LEVEL_9 = 9;
    public final static int LEVEL_10 = 10;
    public final static int LEVEL_11 = 11;
    public final static int LEVEL_12 = 12;
    public final static int LEVEL_13 = 13;
    public final static int LEVEL_14 = 14;
    public final static int LEVEL_15 = 15;
    public final static int LEVEL_16 = 16;
    public final static int LEVEL_17 = 17;
    public final static int LEVEL_18 = 18;
    public final static int LEVEL_19 = 19;
    public final static int LEVEL_20 = 20;

    //Non-Biodegradable
    public final static int[] NB = {
            R.drawable.b32,
            R.drawable.b33,
            R.drawable.b34,
            R.drawable.b35,
            R.drawable.b36,
            R.drawable.b10,
            R.drawable.b11,
            R.drawable.b12,
            R.drawable.b13
    };

    //Biodegradable
    public final static int[] B = {
            R.drawable.b24,
            R.drawable.b25,
            R.drawable.b26,
            R.drawable.b27,
            R.drawable.b28,
            R.drawable.b29,
            R.drawable.b30,
            R.drawable.b31,
            R.drawable.b1,
            R.drawable.b2,
            R.drawable.b3,
            R.drawable.b4,
            R.drawable.b5,
            R.drawable.b6,
            R.drawable.b7,
            R.drawable.b8,
            R.drawable.b9
    };

    //Recycleable
    public final static int[] RB = {
            R.drawable.b16,
            R.drawable.b17,
            R.drawable.b18,
            R.drawable.b19,
            R.drawable.b20,
            R.drawable.b21,
            R.drawable.b22,
            R.drawable.b23,
            R.drawable.b14,
            R.drawable.b15
    };

    //trivia
    public final static int[] TRIVIA = {
            R.drawable.t1,
            R.drawable.t2,
            R.drawable.t3,
            R.drawable.t4,
            R.drawable.t5,
            R.drawable.t6,
            R.drawable.t7,
            R.drawable.t8
    };
}
