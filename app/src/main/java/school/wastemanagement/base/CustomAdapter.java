package school.wastemanagement.base;

import android.app.Activity;
import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import school.wastemanagement.R;
import school.wastemanagement.model.GarbageModel;

public class CustomAdapter extends BaseAdapter{
    Context context;
    List<GarbageModel> datalist = new ArrayList<>();

    public CustomAdapter(Context context, List<GarbageModel> datalist){
        this.context = context;
        this.datalist = datalist;
    }
    @Override
    public int getCount() {
        return datalist.size();
    }
    @Override
    public long getItemId(int position) {
        return 0;
    }
    @Override
    public Object getItem(int position) {
        return null;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        ImageView images = new ImageView(context);
        GarbageModel item = datalist.get(position);
        images.setImageDrawable(((Activity) context).getResources().getDrawable(item.getSrc()));
        return images;
    }

    static class RecordHolder {
        ImageView imageItem;
    }
}
