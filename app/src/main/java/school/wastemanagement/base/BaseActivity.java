package school.wastemanagement.base;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;

public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadLayout();
        ButterKnife.bind(this);
        loadPresenter();
        loadData();
    }

    @Override
    protected void onStart(){
        super.onStart();
    }

    @Override
    protected void onRestart(){
        super.onRestart();
    }

    @Override
    protected void onResume(){
        super.onResume();
    }

    @Override
    protected void onPause(){
        super.onPause();
        if(Dj.isKeepMusicOn()){
            Dj.iAmLeaving();
        }
    }

    @Override
    protected void onStop(){
        super.onStop();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        if(Dj.isKeepMusicOn()) {
            Dj.iAmLeaving();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public Context getContext() {
        return this;
    }

    protected abstract void loadLayout();
    protected abstract void loadPresenter();
    protected abstract void loadData();
}
