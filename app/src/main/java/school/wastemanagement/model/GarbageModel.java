package school.wastemanagement.model;

import org.parceler.Parcel;

@Parcel
public class GarbageModel {
    private int src = 0;
    private String type = "";
    private int sort = 0;

    public GarbageModel() {
    }

    public GarbageModel(int src, String type) {
        this.src = src;
        this.type = type;
    }

    public int getSrc() {
        return src;
    }

    public void setSrc(int src) {
        this.src = src;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }
}
