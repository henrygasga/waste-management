package school.wastemanagement.model;

import org.parceler.Parcel;

import school.wastemanagement.base.Constant;

@Parcel
public class LevelModel {
    private int level = 1;
    private int energy = 0;
    private boolean kidShow = false;
    private int kidCount = 1;
    private boolean withTime = false;
    private boolean isAnyOrder = false;
    private boolean showCloud = false;
    private boolean withGrass = false;
    private boolean withSquare = false;
    private boolean showEnergy = false;
    private int garbageCount = 1;
    private String garbageType = Constant.GARBAGE_ALL;
    private int timeGarbageDisappear = 5000;
    private int timeLimit = 11000;
    private int background = 1;
    private GarbageModel c1;
    private GarbageModel c2;
    private GarbageModel c3;
    private GarbageModel c4;
    private GarbageModel c5;
    private GarbageModel c6;
    private GarbageModel c7;
    private GarbageModel c8;

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public boolean isKidShow() {
        return kidShow;
    }

    public void setKidShow(boolean kidShow) {
        this.kidShow = kidShow;
    }

    public int getKidCount() {
        return kidCount;
    }

    public void setKidCount(int kidCount) {
        this.kidCount = kidCount;
    }

    public boolean isWithTime() {
        return withTime;
    }

    public void setWithTime(boolean withTime) {
        this.withTime = withTime;
    }

    public int getGarbageCount() {
        return garbageCount;
    }

    public void setGarbageCount(int garbageCount) {
        this.garbageCount = garbageCount;
    }

    public String getGarbageType() {
        return garbageType;
    }

    public void setGarbageType(String garbageType) {
        this.garbageType = garbageType;
    }

    public int getTimeGarbageDisappear() {
        return timeGarbageDisappear;
    }

    public void setTimeGarbageDisappear(int timeGarbageDisappear) {
        this.timeGarbageDisappear = timeGarbageDisappear;
    }

    public int getBackground() {
        return background;
    }

    public void setBackground(int background) {
        this.background = background;
    }

    public int getTimeLimit() {
        return timeLimit;
    }

    public void setTimeLimit(int timeLimit) {
        this.timeLimit = timeLimit;
    }

    public boolean isShowCloud() {
        return showCloud;
    }

    public void setShowCloud(boolean showCloud) {
        this.showCloud = showCloud;
    }

    public GarbageModel getC1() {
        return c1;
    }

    public void setC1(GarbageModel c1) {
        this.c1 = c1;
    }

    public GarbageModel getC2() {
        return c2;
    }

    public void setC2(GarbageModel c2) {
        this.c2 = c2;
    }

    public GarbageModel getC3() {
        return c3;
    }

    public void setC3(GarbageModel c3) {
        this.c3 = c3;
    }

    public GarbageModel getC4() {
        return c4;
    }

    public void setC4(GarbageModel c4) {
        this.c4 = c4;
    }

    public GarbageModel getC5() {
        return c5;
    }

    public void setC5(GarbageModel c5) {
        this.c5 = c5;
    }

    public GarbageModel getC6() {
        return c6;
    }

    public void setC6(GarbageModel c6) {
        this.c6 = c6;
    }

    public GarbageModel getC7() {
        return c7;
    }

    public void setC7(GarbageModel c7) {
        this.c7 = c7;
    }

    public GarbageModel getC8() {
        return c8;
    }

    public void setC8(GarbageModel c8) {
        this.c8 = c8;
    }

    public boolean isAnyOrder() {
        return isAnyOrder;
    }

    public void setAnyOrder(boolean anyOrder) {
        isAnyOrder = anyOrder;
    }

    public int getEnergy() {
        return energy;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }

    public boolean isWithGrass() {
        return withGrass;
    }

    public void setWithGrass(boolean withGrass) {
        this.withGrass = withGrass;
    }

    public boolean isWithSquare() {
        return withSquare;
    }

    public void setWithSquare(boolean withSquare) {
        this.withSquare = withSquare;
    }

    public boolean isShowEnergy() {
        return showEnergy;
    }

    public void setShowEnergy(boolean showEnergy) {
        this.showEnergy = showEnergy;
    }
}
