package school.wastemanagement.view;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import school.wastemanagement.base.MyShared;

public class MyBroad extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        context.startService(new Intent(context, EnergyService.class));
    }
}
