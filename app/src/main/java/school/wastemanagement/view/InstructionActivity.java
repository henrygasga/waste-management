package school.wastemanagement.view;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;

import butterknife.BindView;
import butterknife.OnClick;
import school.wastemanagement.R;
import school.wastemanagement.base.BaseActivity;

public class InstructionActivity extends BaseActivity {

    public static final String DID = "DID";

    @BindView(R.id.bg) ConstraintLayout bg;

    @Override
    protected void loadLayout() {
        setContentView(R.layout.activity_instruction);
    }

    @Override
    protected void loadPresenter() {

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void loadData() {
        Bundle extras = getIntent().getExtras();
        bg.setBackground(getResources().getDrawable(extras.getInt(DID)));
    }

    @OnClick(R.id.gotit)
    public void closeInstruction() {
        finish();
    }
}
