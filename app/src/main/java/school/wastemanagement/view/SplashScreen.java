package school.wastemanagement.view;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.nio.channels.AlreadyBoundException;

import butterknife.BindView;
import school.wastemanagement.R;
import school.wastemanagement.base.BaseActivity;

public class SplashScreen extends BaseActivity {

    @Nullable
    @BindView(R.id.iv_logo) ImageView ivLogo;

    private Animation animateLogoPaycent;

    @Override
    protected void loadLayout() {
        setContentView(R.layout.activity_splashscreen);
    }

    @Override
    protected void loadPresenter() {

    }

    @Override
    protected void loadData() {
//        if(ivLogo != null) {
//            Picasso.with(this).load(R.drawable.iv_logo).into(ivLogo);
//            animateLogoPaycent = AnimationUtils.loadAnimation(getContext(), R.anim.fade);
//
//            ivLogo.setAnimation(animateLogoPaycent);
//        }

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashScreen.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }, 3000);
    }

    public static void registerAlarm(Context context){
//        Intent i = new Intent(context, MainActivity.class);
//        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 111,i,0);
//        long first = SystemClock.elapsedRealtime();
//        first += 3 * 1000;
//
//        AlarmManager am = (AlarmManager) context.getSystemService(ALARM_SERVICE);
//        am.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, first, 1000, pendingIntent);
        // 600000 10mins
    }
}
