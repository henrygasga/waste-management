package school.wastemanagement.view;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import org.parceler.Parcels;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import school.wastemanagement.R;
import school.wastemanagement.base.BaseActivity;
import school.wastemanagement.base.Constant;
import school.wastemanagement.base.Dj;
import school.wastemanagement.base.MyShared;
import school.wastemanagement.base.Singleton;
import school.wastemanagement.model.LevelModel;
import school.wastemanagement.view.level.Garbage10Activity;
import school.wastemanagement.view.level.LevelActivity;

public class MainActivity extends BaseActivity {
    Intent mServiceIntent;
    private EnergyService mSensorService;
    LevelModel levelModel = new LevelModel();
    public static String LEVEL_INFO = "LEVEL_INFO";
    public static String IS_LIST = "IS_LIST";

    @Override
    protected void loadLayout() {
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void loadPresenter() {

    }

    @Override
    protected void loadData() {

        MyShared.getSharedPreferences(getContext());

        mSensorService = new EnergyService(this);
        mServiceIntent = new Intent(getApplicationContext(), mSensorService.getClass());
        if (!isMyServiceRunning(mSensorService.getClass())) {
            startService(mServiceIntent);
        }
//        Toast.makeText(this, "e=" + MyShared.getEnergy(getContext()), Toast.LENGTH_SHORT).show();
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i ("isMyServiceRunning?", true+"");
                return true;
            }
        }
        Log.i ("isMyServiceRunning?", false+"");
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        //uncomment to play music
        Dj.iAmIn(getContext());
    }

    @Override
    public void onPause(){
        super.onPause();
        //uncomment this
        Dj.iAmLeaving();
    }

    @Override
    protected void onDestroy() {
        stopService(mServiceIntent);
        Log.i("MAINACT", "onDestroy!");
        super.onDestroy();
        Dj.iAmLeaving();
    }

    @OnClick(R.id.btn_playgame)
    public void startGame() {
        Dj.KeepMusicOn();
        if (MyShared.getEnergy(this) > 0) {
            levelModel.setLevel(Constant.LEVEL_1);
            levelModel.setKidShow(true);
            levelModel.setKidCount(2);
            levelModel.setGarbageType(Constant.GARBAGE_ALL);
//        Bundle bundle = new Bundle();
//        bundle.putParcelable(LEVEL_INFO, Parcels.wrap(levelModel));
            Intent intent = new Intent(this, LevelListActivity.class);
            intent.putExtra(LEVEL_INFO, Parcels.wrap(levelModel));
            startActivity(intent);
        } else {
            Toast.makeText(this, "Restart app to reset Energy.", Toast.LENGTH_SHORT).show();
        }

    }
}
