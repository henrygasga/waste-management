package school.wastemanagement.view;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.annotation.Nullable;

import java.util.Timer;
import java.util.TimerTask;

import school.wastemanagement.base.Constant;
import school.wastemanagement.base.MyShared;

/**
 * Created by noc-894 on 07/02/2018.
 */

public class EnergyService extends Service {
    Context mContext;
    public EnergyService(Context context) {
        super();
        mContext = context;

    }

    public EnergyService() {

    }

    @Override
    public void onCreate() {
        super.onCreate(); // if you override onCreate(), make sure to call super().
        // If a Context object is needed, call getApplicationContext() here.
        MyShared.getSharedPreferences(getApplicationContext());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        startTimer();
        return START_STICKY;
    }

    public void startTimer() {

        Timer myTimer = new Timer();
        myTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                int energy = MyShared.getEnergy(getApplicationContext()) + 1;
                if (energy > 15) {
                    energy = 15;
                }
                MyShared.setEnergy(getApplicationContext(), energy);
            }
        }, 0, 300000);

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
