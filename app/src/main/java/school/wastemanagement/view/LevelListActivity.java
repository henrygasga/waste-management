package school.wastemanagement.view;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import org.parceler.Parcels;

import butterknife.BindView;
import butterknife.OnClick;
import school.wastemanagement.R;
import school.wastemanagement.base.BaseActivity;
import school.wastemanagement.base.Constant;
import school.wastemanagement.base.MyShared;
import school.wastemanagement.base.Singleton;
import school.wastemanagement.model.LevelModel;
import school.wastemanagement.view.level.Garbage10Activity;
import school.wastemanagement.view.level.Garbage20Activity;
import school.wastemanagement.view.level.LevelActivity;

import static school.wastemanagement.view.MainActivity.IS_LIST;
import static school.wastemanagement.view.MainActivity.LEVEL_INFO;

public class LevelListActivity extends BaseActivity {

    @BindView(R.id.lvl1) ImageView lvl1;
    @BindView(R.id.lvl2) ImageView lvl2;
    @BindView(R.id.lvl3) ImageView lvl3;
    @BindView(R.id.lvl4) ImageView lvl4;
    @BindView(R.id.lvl5) ImageView lvl5;
    @BindView(R.id.lvl6) ImageView lvl6;
    @BindView(R.id.lvl7) ImageView lvl7;
    @BindView(R.id.lvl8) ImageView lvl8;
    @BindView(R.id.lvl9) ImageView lvl9;
    @BindView(R.id.lvl10) ImageView lvl10;
    @BindView(R.id.lvl11) ImageView lvl11;
    @BindView(R.id.lvl12) ImageView lvl12;
    @BindView(R.id.lvl13) ImageView lvl13;
    @BindView(R.id.lvl14) ImageView lvl14;
    @BindView(R.id.lvl15) ImageView lvl15;
    @BindView(R.id.lvl16) ImageView lvl16;
    @BindView(R.id.lvl17) ImageView lvl17;
    @BindView(R.id.lvl18) ImageView lvl18;
    @BindView(R.id.lvl19) ImageView lvl19;
    @BindView(R.id.lvl20) ImageView lvl20;

    @BindView(R.id.locl2) ImageView lock2;
    @BindView(R.id.locl3) ImageView lock3;
    @BindView(R.id.locl4) ImageView lock4;
    @BindView(R.id.locl5) ImageView lock5;
    @BindView(R.id.locl6) ImageView lock6;
    @BindView(R.id.locl7) ImageView lock7;
    @BindView(R.id.lock8) ImageView lock8;
    @BindView(R.id.locl9) ImageView lock9;
    @BindView(R.id.locl10) ImageView lock10;
    @BindView(R.id.locl11) ImageView lock11;
    @BindView(R.id.locl12) ImageView lock12;
    @BindView(R.id.locl13) ImageView lock13;
    @BindView(R.id.locl14) ImageView lock14;
    @BindView(R.id.locl15) ImageView lock15;
    @BindView(R.id.locl16) ImageView lock16;
    @BindView(R.id.locl17) ImageView lock17;
    @BindView(R.id.locl18) ImageView lock18;
    @BindView(R.id.locl19) ImageView lock19;
    @BindView(R.id.locl20) ImageView lock20;

    LevelModel levelModel = new LevelModel();

    @Override
    protected void loadLayout() {
        setContentView(R.layout.levellist_activity);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void loadPresenter() {

    }

    @Override
    protected void loadData() {
        MyShared.getSharedPreferences(getContext());
        checkFinishLevel();
    }

    @OnClick(R.id.img_home)
    public void backHome() {
        finish();
    }

    @OnClick(R.id.lvl1)
    public void lvl1() {
        levelModel.setLevel(Constant.LEVEL_1);
        levelModel.setKidShow(true);
        levelModel.setKidCount(2);
        levelModel.setGarbageType(Constant.GARBAGE_ALL);
        Intent intent = new Intent(this, Garbage10Activity.class);
        intent.putExtra(LEVEL_INFO, Parcels.wrap(levelModel));
        intent.putExtra(IS_LIST, true);
        startActivity(intent);

    }

    @Override
    public void onResume(){
        super.onResume();
        checkFinishLevel();
    }

    public void checkFinishLevel() {
        if (MyShared.isLevel2(getContext())) lock2.setVisibility(View.GONE);
        if (MyShared.isLevel3(getContext())) lock3.setVisibility(View.GONE);
        if (MyShared.isLevel4(getContext())) lock4.setVisibility(View.GONE);
        if (MyShared.isLevel5(getContext())) lock5.setVisibility(View.GONE);
        if (MyShared.isLevel6(getContext())) lock6.setVisibility(View.GONE);
        if (MyShared.isLevel7(getContext())) lock7.setVisibility(View.GONE);
        if (MyShared.isLevel8(getContext())) lock8.setVisibility(View.GONE);
        if (MyShared.isLevel9(getContext())) lock9.setVisibility(View.GONE);
        if (MyShared.isLevel10(getContext())) lock10.setVisibility(View.GONE);
        if (MyShared.isLevel11(getContext())) lock11.setVisibility(View.GONE);
        if (MyShared.isLevel12(getContext())) lock12.setVisibility(View.GONE);
        if (MyShared.isLevel13(getContext())) lock13.setVisibility(View.GONE);
        if (MyShared.isLevel14(getContext())) lock14.setVisibility(View.GONE);
        if (MyShared.isLevel15(getContext())) lock15.setVisibility(View.GONE);
        if (MyShared.isLevel16(getContext())) lock16.setVisibility(View.GONE);
        if (MyShared.isLevel17(getContext())) lock17.setVisibility(View.GONE);
        if (MyShared.isLevel18(getContext())) lock18.setVisibility(View.GONE);
        if (MyShared.isLevel19(getContext())) lock19.setVisibility(View.GONE);
        if (MyShared.isLevel20(getContext())) lock20.setVisibility(View.GONE);
    }

    @OnClick({R.id.lvl2,R.id.lvl3,R.id.lvl4,R.id.lvl5,R.id.lvl6,R.id.lvl7,R.id.lvl8,R.id.lvl9,R.id.lvl10,R.id.lvl11,R.id.lvl12,R.id.lvl13,R.id.lvl14,R.id.lvl15,R.id.lvl16,R.id.lvl17,R.id.lvl18,R.id.lvl19,R.id.lvl20})
    public void lvl2(View v) {
        Intent intent = null;
        switch (v.getId()){
            case R.id.lvl2:
                if(MyShared.isLevel2(getContext())){
                    rules(2);
                    intent = new Intent(this, Garbage10Activity.class);
                } else {
                    Toast.makeText(this, "Please finish previous level!", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.lvl3:
                if(MyShared.isLevel3(getContext())){
                    rules(3);
                    intent = new Intent(this, Garbage10Activity.class);
                } else {
                    Toast.makeText(this, "Please finish previous level!", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.lvl4:
                if(MyShared.isLevel4(getContext())){
                    rules(4);
                    intent = new Intent(this, Garbage10Activity.class);
                } else {
                    Toast.makeText(this, "Please finish previous level!", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.lvl5:
                if(MyShared.isLevel5(getContext())){
                    rules(5);
                    intent = new Intent(this, Garbage10Activity.class);
                } else {
                    Toast.makeText(this, "Please finish previous level!", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.lvl6:
                if(MyShared.isLevel6(getContext())){
                    rules(6);
                    intent = new Intent(this, Garbage10Activity.class);
                } else {
                    Toast.makeText(this, "Please finish previous level!", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.lvl7:
                if(MyShared.isLevel7(getContext())){
                    rules(7);
                    intent = new Intent(this, Garbage10Activity.class);
                } else {
                    Toast.makeText(this, "Please finish previous level!", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.lvl8:
                if(MyShared.isLevel8(getContext())){
                    rules(8);
                    intent = new Intent(this, Garbage10Activity.class);
                } else {
                    Toast.makeText(this, "Please finish previous level!", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.lvl9:
                if(MyShared.isLevel9(getContext())){
                    rules(9);
                    intent = new Intent(this, Garbage10Activity.class);
                } else {
                    Toast.makeText(this, "Please finish previous level!", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.lvl10:
                if(MyShared.isLevel10(getContext())){
                    rules(10);
                    intent = new Intent(this, Garbage10Activity.class);
                } else {
                    Toast.makeText(this, "Please finish previous level!", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.lvl11:
                if(MyShared.isLevel11(getContext())){
                    rules(11);
                    intent = new Intent(this, Garbage20Activity.class);
                } else {
                    Toast.makeText(this, "Please finish previous level!", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.lvl12:
                if(MyShared.isLevel12(getContext())){
                    rules(12);
                    intent = new Intent(this, Garbage20Activity.class);
                } else {
                    Toast.makeText(this, "Please finish previous level!", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.lvl13:
                if(MyShared.isLevel13(getContext())){
                    rules(13);
                    intent = new Intent(this, Garbage20Activity.class);
                } else {
                    Toast.makeText(this, "Please finish previous level!", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.lvl14:
                if(MyShared.isLevel14(getContext())){
                    rules(14);
                    intent = new Intent(this, Garbage20Activity.class);
                } else {
                    Toast.makeText(this, "Please finish previous level!", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.lvl15:
                if(MyShared.isLevel15(getContext())){
                    rules(15);
                    intent = new Intent(this, Garbage20Activity.class);
                } else {
                    Toast.makeText(this, "Please finish previous level!", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.lvl16:
                if(MyShared.isLevel16(getContext())){
                    rules(16);
                    intent = new Intent(this, Garbage20Activity.class);
                } else {
                    Toast.makeText(this, "Please finish previous level!", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.lvl17:
                if(MyShared.isLevel17(getContext())){
                    rules(17);
                    intent = new Intent(this, Garbage20Activity.class);
                } else {
                    Toast.makeText(this, "Please finish previous level!", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.lvl18:
                if(MyShared.isLevel18(getContext())){
                    rules(18);
                    intent = new Intent(this, Garbage20Activity.class);
                } else {
                    Toast.makeText(this, "Please finish previous level!", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.lvl19:
                if(MyShared.isLevel19(getContext())){
                    rules(19);
                    intent = new Intent(this, Garbage20Activity.class);
                } else {
                    Toast.makeText(this, "Please finish previous level!", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.lvl20:
                if(MyShared.isLevel20(getContext())){
                    rules(20);
                    intent = new Intent(this, Garbage20Activity.class);
                } else {
                    Toast.makeText(this, "Please finish previous level!", Toast.LENGTH_SHORT).show();
                }
                break;
        }
        if(intent != null) {
            intent.putExtra(LEVEL_INFO, Parcels.wrap(levelModel));
            intent.putExtra(IS_LIST, true);
            startActivity(intent);
        }
    }
    @Override
    public void onPause(){
        super.onPause();
        //uncomment this
    }

    public void rules(int lvl) {
        levelModel = new LevelModel();
        switch (lvl){
            case 1:
                levelModel.setLevel(Constant.LEVEL_1);
                levelModel.setKidShow(true);
                levelModel.setKidCount(2);
                levelModel.setGarbageType(Constant.GARBAGE_ALL);
                break;
            case 2:
                levelModel.setLevel(Constant.LEVEL_2);
                levelModel.setGarbageType(Constant.GARBAGE_ALL);
                break;
            case 3:
                levelModel.setLevel(Constant.LEVEL_3);
                levelModel.setGarbageType(Constant.GARBAGE_ALL);
                levelModel.setWithTime(true);
                levelModel.setTimeLimit(50000);
                break;
            case 4:
                levelModel.setLevel(Constant.LEVEL_4);
                levelModel.setGarbageType(Constant.GARBAGE_ALL);
                levelModel.setWithTime(false);
                levelModel.setShowCloud(true);
                break;
            case 5:
                levelModel.setLevel(Constant.LEVEL_5);
                levelModel.setGarbageType(Constant.GARBAGE_ALL);
                levelModel.setWithTime(true);
                levelModel.setTimeLimit(50000);
                levelModel.setShowCloud(true);
                break;
            case 6:
                levelModel.setLevel(Constant.LEVEL_6);
                levelModel.setGarbageType(Constant.GARBAGE_ALL);
                levelModel.setWithTime(true);
                levelModel.setShowCloud(true);
                levelModel.setTimeLimit(25000);//9000
                break;
            case 7:
                levelModel.setLevel(Constant.LEVEL_7);
                levelModel.setGarbageType(Constant.GARBAGE_ALL);
                levelModel.setWithTime(false);
                levelModel.setShowCloud(false);
                break;
            case 8:
                levelModel.setLevel(Constant.LEVEL_8);
                levelModel.setGarbageType(Constant.GARBAGE_ALL);
                levelModel.setWithTime(true);
                break;
            case 9:
                levelModel.setLevel(Constant.LEVEL_9);
                levelModel.setGarbageType(Constant.GARBAGE_BIO);
                levelModel.setWithTime(false);
                levelModel.setShowEnergy(true);
                levelModel.setEnergy(MyShared.getEnergy(this));
                break;
            case 10:
                levelModel.setLevel(Constant.LEVEL_10);
                levelModel.setGarbageType(Constant.GARBAGE_NON_BIO);
                levelModel.setWithTime(true);
                levelModel.setTimeLimit(25000);//11000
                levelModel.setShowEnergy(true);
                levelModel.setEnergy(MyShared.getEnergy(this));
                break;
            case 11:
                levelModel.setLevel(Constant.LEVEL_11);
                levelModel.setGarbageType(Constant.GARBAGE_ALL);
                levelModel.setWithTime(true);
                levelModel.setTimeLimit(25000); //none
                levelModel.setShowEnergy(true);
                levelModel.setEnergy(MyShared.getEnergy(this));
                break;
            case 12:
                levelModel.setLevel(Constant.LEVEL_12);
                levelModel.setGarbageType(Constant.GARBAGE_ALL);
                levelModel.setShowCloud(true);
                levelModel.setGarbageCount(6);
                levelModel.setTimeGarbageDisappear(10000);//10000
                levelModel.setAnyOrder(true);
                levelModel.setShowEnergy(true);
                levelModel.setEnergy(MyShared.getEnergy(this));
                break;
            case 13:
                levelModel.setLevel(Constant.LEVEL_13);
                levelModel.setGarbageType(Constant.GARBAGE_ALL);
                levelModel.setShowCloud(true);
                levelModel.setGarbageCount(6);
                levelModel.setTimeGarbageDisappear(10000);//10000
                levelModel.setAnyOrder(false);
                levelModel.setShowEnergy(true);
                levelModel.setEnergy(MyShared.getEnergy(this));
                break;
            case 14:
                levelModel.setLevel(Constant.LEVEL_14);
                levelModel.setGarbageType(Constant.GARBAGE_ALL);
                levelModel.setShowCloud(true);
                levelModel.setGarbageCount(5);
                levelModel.setWithGrass(true);
                levelModel.setShowEnergy(true);
                levelModel.setEnergy(MyShared.getEnergy(this));
                break;
            case 15:
                levelModel.setLevel(Constant.LEVEL_15);
                levelModel.setGarbageType(Constant.GARBAGE_ALL);
                levelModel.setShowCloud(true);
                levelModel.setWithTime(true);
                levelModel.setGarbageCount(7);
                levelModel.setWithGrass(true);
                levelModel.setTimeLimit(25000);//10000
                levelModel.setShowEnergy(true);
                levelModel.setEnergy(MyShared.getEnergy(this));
                break;
            case 16:
                levelModel.setLevel(Constant.LEVEL_16);
                levelModel.setGarbageType(Constant.GARBAGE_ALL);
                levelModel.setWithTime(true);
                levelModel.setWithSquare(true);
                levelModel.setTimeLimit(25000);
                levelModel.setTimeGarbageDisappear(5000);
                levelModel.setGarbageCount(5);
                levelModel.setShowEnergy(true);
                levelModel.setEnergy(MyShared.getEnergy(this));
                break;
            case 17:
                levelModel.setLevel(Constant.LEVEL_17);
                levelModel.setGarbageType(Constant.GARBAGE_ALL);
                levelModel.setWithTime(true);
                levelModel.setTimeLimit(25000);//17000
                levelModel.setGarbageCount(10);
                levelModel.setShowEnergy(true);
                levelModel.setEnergy(MyShared.getEnergy(this));
                break;
            case 18:
                levelModel.setLevel(Constant.LEVEL_18);
                levelModel.setGarbageType(Constant.GARBAGE_ALL);
                levelModel.setWithTime(true);
                levelModel.setTimeLimit(25000);//14000
                levelModel.setGarbageCount(13);
                levelModel.setShowEnergy(true);
                levelModel.setEnergy(MyShared.getEnergy(this));
                break;
            case 19:
                levelModel.setLevel(Constant.LEVEL_19);
                levelModel.setGarbageType(Constant.GARBAGE_ALL);
                levelModel.setWithTime(true);
                levelModel.setTimeLimit(25000);//10000
                levelModel.setGarbageCount(16);
                levelModel.setShowEnergy(true);
                levelModel.setEnergy(MyShared.getEnergy(this));
                break;
            case 20:
                levelModel.setLevel(Constant.LEVEL_20);
                levelModel.setGarbageType(Constant.GARBAGE_ALL);
                levelModel.setGarbageCount(20);
                levelModel.setShowEnergy(true);
                levelModel.setEnergy(MyShared.getEnergy(this));
                break;
        }
    }

}
