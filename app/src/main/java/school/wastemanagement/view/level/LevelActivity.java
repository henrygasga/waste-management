package school.wastemanagement.view.level;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.Intent;
import android.os.Build;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import butterknife.BindView;
import butterknife.OnClick;
import school.wastemanagement.R;
import school.wastemanagement.base.BaseActivity;
import school.wastemanagement.base.Constant;
import school.wastemanagement.base.CustomAdapter;
import school.wastemanagement.base.DialogFactory;
import school.wastemanagement.base.Singleton;
import school.wastemanagement.base.SquareLayout;
import school.wastemanagement.base.Utils;
import school.wastemanagement.model.GarbageModel;
import school.wastemanagement.model.LevelModel;
import school.wastemanagement.presenter.GarbageContract;
import school.wastemanagement.presenter.GarbagePresenter;
import school.wastemanagement.view.LevelListActivity;
import school.wastemanagement.view.MainActivity;

import static school.wastemanagement.view.MainActivity.LEVEL_INFO;

public class LevelActivity extends BaseActivity implements GarbageContract.View{

    LevelModel levelModel;
    ProgressDialog progressDialog;
    GarbageContract.Presenter presenter;
    int cloudItems = 0;
    CountDownTimer downTimer;
    boolean isRunning = false;

    @BindView(R.id.img_level) ImageView imgLevel;
    @BindView(R.id.tv_time) TextView tvTime;
    @BindView(R.id.level_bg) LinearLayout levelBg;
    @BindView(R.id.ll_clouds) LinearLayout llClouds;
    @BindView(R.id.c1) ImageView cloud1;
    @BindView(R.id.c2) ImageView cloud2;
    @BindView(R.id.c3) ImageView cloud3;
    @BindView(R.id.c4) ImageView cloud4;
    @BindView(R.id.c5) ImageView cloud5;
    @BindView(R.id.c6) ImageView cloud6;
    @BindView(R.id.c7) ImageView cloud7;
    @BindView(R.id.c8) ImageView cloud8;

    ImageView imgGar1;


    //garbage
    @BindView(R.id.img_nonbio) LinearLayout nonBio;
    @BindView(R.id.img_bio) LinearLayout bio;
    @BindView(R.id.img_recycle) LinearLayout recycle;

    //grade 1
    @BindView(R.id.ll_grade1) LinearLayout llGrade1;
    @BindView(R.id.g1b1) ImageView g1b1;
    @BindView(R.id.g1b2) ImageView g1b2;
    int g1DropCount = 0;

    //grade 2
    @BindView(R.id.ll_grade2) LinearLayout llGrade2;
    @BindView(R.id.g2b1) ImageView g2b1;
    @BindView(R.id.g2b2) ImageView g2b2;
    @BindView(R.id.g2b3) ImageView g2b3;
    @BindView(R.id.g2b4) ImageView g2b4;
    int g2DropCount = 0;

    //grade 3
    @BindView(R.id.ll_grade3) LinearLayout llGrade3;
    @BindView(R.id.g3b1) ImageView g3b1;
    @BindView(R.id.g3b2) ImageView g3b2;
    @BindView(R.id.g3b3) ImageView g3b3;
    @BindView(R.id.g3b4) ImageView g3b4;
    int g3DropCount = 0;



    @Override
    protected void loadLayout() {
        levelModel = Parcels.unwrap(getIntent().getParcelableExtra(MainActivity.LEVEL_INFO));
        setContentView(R.layout.activity_level);
        progressDialog = DialogFactory.createProgressDialog(this, "Loading...");
    }

    @Override
    protected void loadPresenter() {
        presenter = new GarbagePresenter(this);
    }

    @Override
    protected void loadData() {
        setUpGame();
        nonBio.setOnDragListener(new MyDragListener());
        bio.setOnDragListener(new MyDragListener());
        recycle.setOnDragListener(new MyDragListener());
    }

    @OnClick(R.id.img_home)
    public void backHome() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    public void setUpGame() {
        if(!progressDialog.isShowing()) progressDialog.show();
        setGrade(levelModel.getLevel());
        setGameView(levelModel.getLevel());

        if(progressDialog.isShowing()) progressDialog.dismiss();
    }

    public void timesUp() {
        if(!((Activity) getContext()).isFinishing()) {
            DialogFactory.createColoredDialog(getContext(), "Failed!", new DialogFactory.DialogButtonOnClickListener() {
                @Override
                public void onPositiveButtonClick() {
                    if(isRunning) {
                        isRunning = false;
                        downTimer.cancel();
                    }
                    finish();
                }
            }).show();
        }
    }

    public void setGameView(int grade) {

        switch (grade) {
            case Constant.LEVEL_1:
                llGrade1.setVisibility(View.VISIBLE);
                g1b1.setOnTouchListener(new MyTouchListener());
                g1b2.setOnTouchListener(new MyTouchListener());
                presenter.generate(Constant.GARBAGE_ALL,2);
                break;
            case Constant.LEVEL_2:
                llGrade1.setVisibility(View.GONE);
                llGrade2.setVisibility(View.VISIBLE);
                g2b1.setOnTouchListener(new MyTouchListener());
                g2b2.setOnTouchListener(new MyTouchListener());
                g2b3.setOnTouchListener(new MyTouchListener());
                g2b4.setOnTouchListener(new MyTouchListener());
                break;
            case Constant.LEVEL_3:
                llGrade2.setVisibility(View.GONE);
                llGrade3.setVisibility(View.VISIBLE);
                tvTime.setVisibility(View.VISIBLE);
                g3b1.setOnTouchListener(new MyTouchListener());
                g3b2.setOnTouchListener(new MyTouchListener());
                g3b3.setOnTouchListener(new MyTouchListener());
                g3b4.setOnTouchListener(new MyTouchListener());
                break;
            case Constant.LEVEL_4:
                llGrade3.setVisibility(View.GONE);
                tvTime.setVisibility(View.GONE);
                llClouds.setVisibility(View.VISIBLE);
                presenter.generateCloud(3);
                break;
            case Constant.LEVEL_5:
                llClouds.setVisibility(View.VISIBLE);
                tvTime.setVisibility(View.VISIBLE);
                presenter.generateCloud(5);
                break;
            case Constant.LEVEL_6:
                llClouds.setVisibility(View.VISIBLE);
                tvTime.setVisibility(View.VISIBLE);
                presenter.generateCloud(8);
                break;
            case Constant.LEVEL_7:

                break;
            case Constant.LEVEL_8:

                break;
            case Constant.LEVEL_9:

                break;
            case Constant.LEVEL_10:

                break;
            case Constant.LEVEL_11:

                break;
            case Constant.LEVEL_12:

                break;
            case Constant.LEVEL_13:

                break;
            case Constant.LEVEL_14:

                break;
            case Constant.LEVEL_15:

                break;
            case Constant.LEVEL_16:

                break;
            case Constant.LEVEL_17:

                break;
            case Constant.LEVEL_18:

                break;
            case Constant.LEVEL_19:

                break;
            case Constant.LEVEL_20:

                break;
        }
    }


    @Override
    public void garbageList(List<GarbageModel> garbageGenerated) {
        Map<Integer, GarbageModel> guideGar = new HashMap<>();
        switch (garbageGenerated.size()) {
            case 2:
                guideGar.put(R.id.g1b1,garbageGenerated.get(0));
                g1b1.setVisibility(View.VISIBLE);
                g1b1.setImageDrawable(getResources().getDrawable(garbageGenerated.get(0).getSrc()));
                guideGar.put(R.id.g1b2,garbageGenerated.get(1));
                g1b2.setVisibility(View.VISIBLE);
                g1b2.setImageDrawable(getResources().getDrawable(garbageGenerated.get(1).getSrc()));
                break;
            case 4:
                switch (levelModel.getLevel()){
                    case 2:
                        guideGar.put(R.id.g2b1,garbageGenerated.get(0));
                        g2b1.setVisibility(View.VISIBLE);
                        g2b1.setImageDrawable(getResources().getDrawable(garbageGenerated.get(0).getSrc()));
                        guideGar.put(R.id.g2b2,garbageGenerated.get(1));
                        g2b2.setVisibility(View.VISIBLE);
                        g2b2.setImageDrawable(getResources().getDrawable(garbageGenerated.get(1).getSrc()));
                        guideGar.put(R.id.g2b3,garbageGenerated.get(2));
                        g2b3.setVisibility(View.VISIBLE);
                        g2b3.setImageDrawable(getResources().getDrawable(garbageGenerated.get(2).getSrc()));
                        guideGar.put(R.id.g2b4,garbageGenerated.get(2));
                        g2b4.setVisibility(View.VISIBLE);
                        g2b4.setImageDrawable(getResources().getDrawable(garbageGenerated.get(2).getSrc()));
                        break;
                    case 3:
                        guideGar.put(R.id.g3b1,garbageGenerated.get(0));
                        g3b1.setVisibility(View.VISIBLE);
                        g3b1.setImageDrawable(getResources().getDrawable(garbageGenerated.get(0).getSrc()));
                        guideGar.put(R.id.g3b2,garbageGenerated.get(1));
                        g3b2.setVisibility(View.VISIBLE);
                        g3b2.setImageDrawable(getResources().getDrawable(garbageGenerated.get(1).getSrc()));
                        guideGar.put(R.id.g3b3,garbageGenerated.get(2));
                        g3b3.setVisibility(View.VISIBLE);
                        g3b3.setImageDrawable(getResources().getDrawable(garbageGenerated.get(2).getSrc()));
                        guideGar.put(R.id.g3b4,garbageGenerated.get(2));
                        g3b4.setVisibility(View.VISIBLE);
                        g3b4.setImageDrawable(getResources().getDrawable(garbageGenerated.get(2).getSrc()));
                        break;
                }

                break;
        }
        Singleton.getInstance().setGuideGarbage(guideGar);
    }

    @Override
    public void cloudGarbageList(List<GarbageModel> garbageCloudGenerated) {
        cloudItems = garbageCloudGenerated.size();
        Log.d("garbageCloudGenerated", garbageCloudGenerated.size() + " garbage");
        switch (garbageCloudGenerated.size()){
            case 8:
                cloud8.setVisibility(View.VISIBLE);
                cloud8.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(7).getSrc()));
            case 7:
                cloud7.setVisibility(View.VISIBLE);
                cloud7.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(6).getSrc()));
            case 6:
                cloud6.setVisibility(View.VISIBLE);
                cloud6.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(5).getSrc()));
            case 5:
                cloud5.setVisibility(View.VISIBLE);
                cloud5.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(4).getSrc()));
            case 4:
                cloud4.setVisibility(View.VISIBLE);
                cloud4.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(3).getSrc()));
            case 3:
                cloud1.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(0).getSrc()));
                cloud2.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(1).getSrc()));
                cloud3.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(2).getSrc()));
                break;
        }
        switch (levelModel.getLevel()){
            case 6:
                presenter.getCloudPartner(10, garbageCloudGenerated);
                break;
            case 4:
            case 5:
                presenter.getCloudPartner(6, garbageCloudGenerated);
                break;
        }

    }

    @Override
    public void genWithCloud(List<GarbageModel> garbageCloudGenerated) {
        Map<Integer, GarbageModel> guideGar = new HashMap<>();
        Log.d("genWithCloud", GarbagePresenter.getGeneratedCloudList().size() + " garbage");
    }

    private final class MyTouchListener implements View.OnTouchListener{

        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                ClipData data = ClipData.newPlainText("","");
                View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
                view.startDrag(data, shadowBuilder, view, 0);
//                view.setVisibility(View.INVISIBLE);
                return true;
            } else {
                return false;
            }
        }
    }

    public class MyDragListener implements View.OnDragListener {

        @Override
        public boolean onDrag(View view, DragEvent dragEvent) {
            int action = dragEvent.getAction();
            switch (action){
                case DragEvent.ACTION_DRAG_STARTED:
                    break;
                case DragEvent.ACTION_DRAG_ENTERED:
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    break;
                case DragEvent.ACTION_DROP:
                    View v = (View) dragEvent.getLocalState();
                    ViewGroup owner = (ViewGroup) v.getParent();
                    owner.removeView(v);
                    Log.d("V_TAG", view.getTag().toString());
                    processDrop(view.getId(), v, owner);
//                    LinearLayout cont = (LinearLayout) view;
//                    cont.addView(v);
//                    v.bringToFront();
//                    v.setVisibility(View.VISIBLE);
                    break;
                case DragEvent.ACTION_DRAG_ENDED:
                    break;
            }
            return true;
        }
    }

    public void setUpRules() {
        int lvl = levelModel.getLevel() + 1;
        levelModel = new LevelModel();
        levelModel.setLevel(lvl);
        switch (lvl) {
            case 2:
                Singleton.getInstance().setLevel2(true);
                levelModel.setGarbageType(Constant.GARBAGE_ALL);
                break;
            case 3:
                Singleton.getInstance().setLevel3(true);
                levelModel.setGarbageType(Constant.GARBAGE_ALL);
                levelModel.setWithTime(true);
                break;
            case 4:
                Singleton.getInstance().setLevel4(true);
                levelModel.setGarbageType(Constant.GARBAGE_ALL);
                levelModel.setWithTime(false);
                levelModel.setShowCloud(true);
                break;
            case 5:
                levelModel.setGarbageType(Constant.GARBAGE_ALL);
                levelModel.setWithTime(true);
                levelModel.setShowCloud(true);
                break;
            case 6:
                levelModel.setGarbageType(Constant.GARBAGE_ALL);
                levelModel.setWithTime(true);
                levelModel.setShowCloud(true);
                levelModel.setTimeLimit(9000);
                break;
            case 7:
                levelModel.setGarbageType(Constant.GARBAGE_ALL);
                levelModel.setWithTime(false);
                levelModel.setShowCloud(false);
                break;
            case 8:
                levelModel.setGarbageType(Constant.GARBAGE_ALL);
                levelModel.setWithTime(true);
                levelModel.setShowCloud(false);
                break;
            case 9:
                levelModel.setGarbageType(Constant.GARBAGE_BIO);
                levelModel.setWithTime(false);
                break;
            case 10:
                levelModel.setGarbageType(Constant.GARBAGE_NON_BIO);
                levelModel.setWithTime(true);
                break;
            case 11:
                levelModel.setGarbageType(Constant.GARBAGE_REC);
                levelModel.setWithTime(true);
                break;
            case 13:
                levelModel.setAnyOrder(true);
            case 12:
                levelModel.setGarbageType(Constant.GARBAGE_ALL);
                levelModel.setWithTime(false);
                levelModel.setShowCloud(true);
                levelModel.setTimeGarbageDisappear(11000);
                break;
            case 15:
            levelModel.setC6(new GarbageModel(R.drawable.b35, Constant.GARBAGE_REC));
            levelModel.setC7(new GarbageModel(R.drawable.b17, Constant.GARBAGE_REC));
            case 14:
                levelModel.setGarbageType(Constant.GARBAGE_ALL);
                levelModel.setShowCloud(true);
                levelModel.setC1(new GarbageModel(R.drawable.b22, Constant.GARBAGE_REC));
                levelModel.setC2(new GarbageModel(R.drawable.b24, Constant.GARBAGE_BIO));
                levelModel.setC3(new GarbageModel(R.drawable.b12, Constant.GARBAGE_NON_BIO));
                levelModel.setC4(new GarbageModel(R.drawable.b13, Constant.GARBAGE_NON_BIO));
                levelModel.setC5(new GarbageModel(R.drawable.b8, Constant.GARBAGE_BIO));
                break;
            case 16:
                levelModel.setGarbageType(Constant.GARBAGE_ALL);
                levelModel.setShowCloud(false);
                levelModel.setTimeGarbageDisappear(6000);
                levelModel.setGarbageCount(7);
                break;
            case 17:
                levelModel.setGarbageType(Constant.GARBAGE_ALL);
                levelModel.setGarbageCount(10);
                levelModel.setTimeGarbageDisappear(17000);
                break;
            case 18:
                levelModel.setGarbageType(Constant.GARBAGE_ALL);
                levelModel.setGarbageCount(13);
                levelModel.setTimeGarbageDisappear(14000);
                break;
            case 19:
                levelModel.setGarbageType(Constant.GARBAGE_ALL);
                levelModel.setGarbageCount(16);
                levelModel.setTimeGarbageDisappear(10000);
                break;
            case 20:
                levelModel.setGarbageType(Constant.GARBAGE_ALL);
                levelModel.setGarbageCount(20);
                break;
        }
    }

    public void showDialogConfirm(int level) {
        final int l = level + 1;
        Singleton.getInstance().setStopTime(true);
        setUpRules();
        if(levelModel.getLevel() <= 3) {
            setUpGame();
        }
        DialogFactory.createColoredDialog(getContext(), "Success!!", new DialogFactory.DialogButtonOnClickListener() {
            @Override
            public void onPositiveButtonClick() {
                DialogFactory.createColoredDialog(getContext(), "Level " + l, new DialogFactory.DialogButtonOnClickListener() {
                    @Override
                    public void onPositiveButtonClick() {
                        if(levelModel.getLevel() <= 3) {
                            if(levelModel.isWithTime()) {
                                Singleton.getInstance().setStopTime(false);
                                if(isRunning) {
                                    isRunning = false;
                                    downTimer.cancel();
                                }
                                downTimer = new CountDownTimer(levelModel.getTimeLimit(), 1000) {
                                    public void onTick(long millisUntilFinished) {
                                        isRunning = true;
                                        tvTime.setText("TIME: " + millisUntilFinished / 1000);
                                    }
                                    public void onFinish() {
                                        isRunning = false;
                                        if(!Singleton.getInstance().isStopTime()) {
                                            timesUp();
                                        }
                                    }

                                };
                                downTimer.start();
                            }
                        } else {
                            if(isRunning) {
                                isRunning = false;
                                downTimer.cancel();
                            }
                            Intent intent = new Intent(LevelActivity.this, Garbage10Activity.class);
                            intent.putExtra(LEVEL_INFO, Parcels.wrap(levelModel));
                            startActivity(intent);
                            finish();
                        }
                    }
                }).show();
            }
        }).show();
    }

    @Override
    public void onPause(){
        super.onPause();
        //uncomment this
    }

    public void processDrop(int garbageType, View v, ViewGroup vg) {
        switch (levelModel.getLevel()) {
            case 1:
                Map<Integer, GarbageModel> itemsCheck0 = Singleton.getInstance().getGuideGarbage();
                Log.d("DROP_ID", "" + v.getId());
                Set<Integer> keys = itemsCheck0.keySet();
                for(Integer key: keys){
                    Log.d("GAR_ID", "" + key);
                }

                if(itemsCheck0.containsKey(v.getId())) {
                    GarbageModel gm = itemsCheck0.get(v.getId());
                    Log.d("TYPE", "" + gm.getType());
                    Log.d("CAN", "" + Utils.getGType(garbageType));
                    if(!gm.getType().equalsIgnoreCase(Utils.getGType(garbageType))) {
                        vg.addView(v);
                        Utils.soundFail(getContext());
                    } else {
                        Utils.soundWin(getContext());
                        g1DropCount = g1DropCount + 1;
                    }
                } else {
                    Utils.soundFail(getContext());
                    vg.addView(v);
                }
                if(g1DropCount == 2) {

                    Singleton.getInstance().setLevel1(true);
                    showDialogConfirm(levelModel.getLevel());
                }
                break;
            case 2:
                Map<Integer, GarbageModel> itemsCheck3 = Singleton.getInstance().getGuideGarbage();
                Log.d("DROP_ID", "" + v.getId());
                if(itemsCheck3.containsKey(v.getId())) {
                    GarbageModel gm = itemsCheck3.get(v.getId());
                    if(!gm.getType().equalsIgnoreCase(Utils.getGType(garbageType))) {
                        vg.addView(v);
                        Utils.soundFail(getContext());
                    } else {
                        Utils.soundWin(getContext());
                        g2DropCount = g2DropCount + 1;
                    }
                } else {
                    Utils.soundFail(getContext());
                    vg.addView(v);
                }
                if(g2DropCount == 4) {
//                    Singleton.getInstance().setLevel2(true);
                    showDialogConfirm(levelModel.getLevel());
                }
                break;
            case 3:
                Map<Integer, GarbageModel> itemsCheck2 = Singleton.getInstance().getGuideGarbage();
                Log.d("DROP_ID", "" + v.getId());
                if(itemsCheck2.containsKey(v.getId())) {
                    GarbageModel gm = itemsCheck2.get(v.getId());
                    if(!gm.getType().equalsIgnoreCase(Utils.getGType(garbageType))) {
                        vg.addView(v);
                        Utils.soundFail(getContext());
                    } else {
                        Utils.soundWin(getContext());
                        g3DropCount = g3DropCount + 1;
                    }
                } else {
                    Utils.soundFail(getContext());
                    vg.addView(v);
                }
                if(g3DropCount == 4) {
//                    Singleton.getInstance().setLevel3(true);
                    showDialogConfirm(levelModel.getLevel());
                }
                break;
            case 4:

                break;
            case 5:

                break;
            case 6:

                break;
            case 7:
                break;
            case 8:
                break;
            case 9:
                break;
            case 10:
                break;
        }
        switch (garbageType) {
            case R.id.img_nonbio:

                break;
            case R.id.img_bio:

                break;
            case R.id.img_recycle:

                break;
        }
    }

    public String getGType(int type) {
        switch (type) {
            case R.id.img_nonbio:
                return Constant.GARBAGE_NON_BIO;
            case R.id.img_bio:
                return Constant.GARBAGE_BIO;
            case R.id.img_recycle:
                return Constant.GARBAGE_REC;
            default:
                return "";
        }
    }

    public void setGrade(int level) {
        int bg = 0;
        int bgLvl = 0;
        switch (level) {
            case 1:
                bgLvl = R.drawable.iv_lvl1;
                bg = R.drawable.bg1;
                break;
            case 2:
                bgLvl = R.drawable.iv_lvl2;
                bg = R.drawable.bg2;
                break;
            case 3:
                bgLvl = R.drawable.iv_lvl3;
                bg = R.drawable.bg3;
                break;
            case 4:
                bgLvl = R.drawable.iv_lvl4;
                bg = R.drawable.bg4;
                break;
            case 5:
                bgLvl = R.drawable.iv_lvl5;
                bg = R.drawable.bg5;
                break;
            case 6:
                bgLvl = R.drawable.iv_lvl6;
                bg = R.drawable.bg6;
                break;
            case 7:
                bgLvl = R.drawable.iv_lvl7;
                bg = R.drawable.bg7;
                break;
            case 8:
                bgLvl = R.drawable.iv_lvl8;
                bg = R.drawable.bg8;
                break;
            case 9:
                bgLvl = R.drawable.iv_lvl9;
                bg = R.drawable.bg9;
                break;
            case 10:
                bgLvl = R.drawable.iv_lvl10;
                bg = R.drawable.bg1;
                break;
            case 11:
                bgLvl = R.drawable.iv_lvl11;
                bg = R.drawable.bg2;
                break;
            case 12:
                bgLvl = R.drawable.iv_lvl12;
                bg = R.drawable.bg3;
                break;
            case 13:
                bgLvl = R.drawable.iv_lvl13;
                bg = R.drawable.bg4;
                break;
            case 14:
                bgLvl = R.drawable.iv_lvl14;
                bg = R.drawable.bg5;
                break;
            case 15:
                bgLvl = R.drawable.iv_lvl15;
                bg = R.drawable.bg6;
                break;
            case 16:
                bgLvl = R.drawable.iv_lvl16;
                bg = R.drawable.bg7;
                break;
            case 17:
                bgLvl = R.drawable.iv_lvl17;
                bg = R.drawable.bg8;
                break;
            case 18:
                bgLvl = R.drawable.iv_lvl18;
                bg = R.drawable.bg9;
                break;
            case 19:
                bgLvl = R.drawable.iv_lvl19;
                bg = R.drawable.bg1;
                break;
            case 20:
                bgLvl = R.drawable.iv_lvl20;
                bg = R.drawable.bg2;
                break;
                default:
                    imgLevel.setImageDrawable(getResources().getDrawable(R.drawable.iv_lvl1));
        }
        imgLevel.setImageDrawable(getResources().getDrawable(bgLvl));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            levelBg.setBackground(getResources().getDrawable(bg));
        }
    }
}
