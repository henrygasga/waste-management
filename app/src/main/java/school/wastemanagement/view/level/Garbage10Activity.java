package school.wastemanagement.view.level;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.DragEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.parceler.Parcels;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import butterknife.BindView;
import butterknife.OnClick;
import school.wastemanagement.R;
import school.wastemanagement.base.BaseActivity;
import school.wastemanagement.base.Constant;
import school.wastemanagement.base.DialogFactory;
import school.wastemanagement.base.MyDragListener;
import school.wastemanagement.base.MyShared;
import school.wastemanagement.base.MyTouchListener;
import school.wastemanagement.base.Singleton;
import school.wastemanagement.base.TaskFailed;
import school.wastemanagement.base.TaskSuccess;
import school.wastemanagement.base.Utils;
import school.wastemanagement.model.GarbageModel;
import school.wastemanagement.model.LevelModel;
import school.wastemanagement.model.TriviaModel;
import school.wastemanagement.presenter.GarbageContract;
import school.wastemanagement.presenter.GarbagePresenter;
import school.wastemanagement.presenter.InstructionContract;
import school.wastemanagement.presenter.InstructionPresenter;
import school.wastemanagement.presenter.TriviaContract;
import school.wastemanagement.presenter.TriviaPresenter;
import school.wastemanagement.view.InstructionActivity;
import school.wastemanagement.view.MainActivity;

import static school.wastemanagement.view.MainActivity.IS_LIST;
import static school.wastemanagement.view.MainActivity.LEVEL_INFO;

public class Garbage10Activity extends BaseActivity implements GarbageContract.View, MyDragListener.DropListener, TriviaContract.View,
        InstructionContract.View{

    LevelModel levelModel;
    GarbageContract.Presenter presenter;
    TriviaContract.Presenter presenterTrivia;
    InstructionContract.Presenter insPresenter;
    MyDragListener dragListener;
    int cloudItems = 0;
    CountDownTimer downTimer;
    boolean isRunning = false;
    boolean isList = false;


    @BindView(R.id.img_level) ImageView imgLevel;
    @BindView(R.id.tv_time) TextView tvTime;
    @BindView(R.id.tv_energy) TextView tvEnergy;
    @BindView(R.id.level_bg) LinearLayout levelBg;
    @BindView(R.id.ll_clouds) LinearLayout llClouds;
    @BindView(R.id.garbage) LinearLayout garbagess;
    @BindView(R.id.c1) ImageView cloud1;
    @BindView(R.id.c2) ImageView cloud2;
    @BindView(R.id.c3) ImageView cloud3;
    @BindView(R.id.c4) ImageView cloud4;
    @BindView(R.id.c5) ImageView cloud5;
    @BindView(R.id.c6) ImageView cloud6;
    @BindView(R.id.c7) ImageView cloud7;
    @BindView(R.id.c8) ImageView cloud8;

    //grade 1
    @BindView(R.id.ll_grade1) LinearLayout llGrade1;
    @BindView(R.id.g1b1) ImageView g1b1;
    @BindView(R.id.g1b2) ImageView g1b2;
    int g1DropCount = 0;

    //grade 2
    @BindView(R.id.ll_grade2) LinearLayout llGrade2;
    @BindView(R.id.g2b1) ImageView g2b1;
    @BindView(R.id.g2b2) ImageView g2b2;
    @BindView(R.id.g2b3) ImageView g2b3;
    @BindView(R.id.g2b4) ImageView g2b4;
    int g2DropCount = 0;

    //grade 3
    @BindView(R.id.ll_grade3) LinearLayout llGrade3;
    @BindView(R.id.g3b1) ImageView g3b1;
    @BindView(R.id.g3b2) ImageView g3b2;
    @BindView(R.id.g3b3) ImageView g3b3;
    @BindView(R.id.g3b4) ImageView g3b4;
    int g3DropCount = 0;

    //garbage 10
    @BindView(R.id.gar1) ImageView gar1;
    @BindView(R.id.gar2) ImageView gar2;
    @BindView(R.id.gar3) ImageView gar3;
    @BindView(R.id.gar4) ImageView gar4;
    @BindView(R.id.gar5) ImageView gar5;
    @BindView(R.id.gar6) ImageView gar6;
    @BindView(R.id.gar7) ImageView gar7;
    @BindView(R.id.gar8) ImageView gar8;
    @BindView(R.id.gar9) ImageView gar9;
    @BindView(R.id.gar10) ImageView gar10;

    //garbage
    @BindView(R.id.img_nonbio) LinearLayout nonBio;
    @BindView(R.id.img_bio) LinearLayout bio;
    @BindView(R.id.img_recycle) LinearLayout recycle;

    int g4DropCount = 0;
    int g5DropCount = 0;
    int g6DropCount = 0;
    int g7DropCount = 0;
    int g8DropCount = 0;
    int g9DropCount = 0;
    int g10DropCount = 0;

    @Override
    protected void loadLayout() {
        levelModel = Parcels.unwrap(getIntent().getParcelableExtra(LEVEL_INFO));
        setContentView(R.layout.activity_garbage10);
        isList = getIntent().getBooleanExtra(IS_LIST, false);

    }

    @Override
    protected void loadPresenter() {
        presenter = new GarbagePresenter(this);
        presenterTrivia = new TriviaPresenter(this);
        insPresenter = new InstructionPresenter(this);
    }

    @Override
    protected void loadData() {
        MyShared.getSharedPreferences(this);
        dragListener = new MyDragListener();
        dragListener.setDropListener(this);
        nonBio.setOnDragListener(dragListener);
        bio.setOnDragListener(dragListener);
        recycle.setOnDragListener(dragListener);
        gar1.setOnTouchListener(new MyTouchListener());
        gar2.setOnTouchListener(new MyTouchListener());
        gar3.setOnTouchListener(new MyTouchListener());
        gar4.setOnTouchListener(new MyTouchListener());
        gar5.setOnTouchListener(new MyTouchListener());
        gar6.setOnTouchListener(new MyTouchListener());
        gar7.setOnTouchListener(new MyTouchListener());
        gar8.setOnTouchListener(new MyTouchListener());
        gar9.setOnTouchListener(new MyTouchListener());
        gar10.setOnTouchListener(new MyTouchListener());

        g1b1.setOnTouchListener(new MyTouchListener());
        g1b2.setOnTouchListener(new MyTouchListener());

        g2b1.setOnTouchListener(new MyTouchListener());
        g2b2.setOnTouchListener(new MyTouchListener());
        g2b3.setOnTouchListener(new MyTouchListener());
        g2b4.setOnTouchListener(new MyTouchListener());

        g3b1.setOnTouchListener(new MyTouchListener());
        g3b2.setOnTouchListener(new MyTouchListener());
        g3b3.setOnTouchListener(new MyTouchListener());
        g3b4.setOnTouchListener(new MyTouchListener());
        setUpGame();
        if(isList) {
//            Toast.makeText(this, "list", Toast.LENGTH_SHORT).show();
            setUpRules(levelModel.getLevel());
        }
    }

    @Override
    public void garbageList(List<GarbageModel> garbageGenerated) {
        switch (levelModel.getLevel()){
            case 10:
            case 9:
            case 8:
            case 7:
                presenter.getCloudPartner(6, garbageGenerated);
                break;
            case 3:
            case 2:
            case 1:
                setUpGarbage(garbageGenerated);
                break;
        }
    }

    @Override
    public void cloudGarbageList(List<GarbageModel> garbageCloudGenerated) {
        cloudItems = garbageCloudGenerated.size();
        Log.d("garbageCloudGenerated", garbageCloudGenerated.size() + " garbage");
        switch (garbageCloudGenerated.size()) {
            case 8:
                cloud8.setVisibility(View.VISIBLE);
                cloud8.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(7).getSrc()));
            case 7:
                cloud7.setVisibility(View.VISIBLE);
                cloud7.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(6).getSrc()));
            case 6:
                cloud6.setVisibility(View.VISIBLE);
                cloud6.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(5).getSrc()));
            case 5:
                cloud5.setVisibility(View.VISIBLE);
                cloud5.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(4).getSrc()));
            case 4:
                cloud4.setVisibility(View.VISIBLE);
                cloud4.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(3).getSrc()));
            case 3:
                cloud1.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(0).getSrc()));
                cloud2.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(1).getSrc()));
                cloud3.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(2).getSrc()));
                break;
        }
        switch (levelModel.getLevel()){
            case 6:
                presenter.getCloudPartner(10, garbageCloudGenerated);
                break;
            case 4:
            case 5:
                presenter.getCloudPartner(6, garbageCloudGenerated);
                break;
        }
    }

    @Override
    public void genWithCloud(List<GarbageModel> garbageCloudGenerated) {
        setUpGarbage(garbageCloudGenerated);
    }

    public void setUpGarbage(List<GarbageModel> garbageCloudGenerated) {
        Map<Integer, GarbageModel> guideGar = new HashMap<>();
//        Log.d("genWithCloud", GarbagePresenter.getGeneratedCloudList().size() + " garbage");
        Log.d("genCloud", garbageCloudGenerated.size() + " garbage");

        switch (garbageCloudGenerated.size()) {
            case 10:
                if(cloudItems >= 10) guideGar.put(R.id.gar10,garbageCloudGenerated.get(9));
                if(garbageCloudGenerated.get(9) != null) gar10.setVisibility(View.VISIBLE);
                gar10.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(9).getSrc()));
            case 9:
                if(cloudItems >= 9) guideGar.put(R.id.gar9,garbageCloudGenerated.get(8));
                if(garbageCloudGenerated.get(8) != null) gar9.setVisibility(View.VISIBLE);
                gar9.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(8).getSrc()));
            case 8:
                guideGar.put(R.id.gar8,garbageCloudGenerated.get(7));
                if(garbageCloudGenerated.get(7) != null) gar8.setVisibility(View.VISIBLE);
                gar8.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(7).getSrc()));
            case 7:
                guideGar.put(R.id.gar7,garbageCloudGenerated.get(6));
                if(garbageCloudGenerated.get(6) != null) gar7.setVisibility(View.VISIBLE);
                gar7.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(6).getSrc()));
            case 6:
                guideGar.put(R.id.gar6,garbageCloudGenerated.get(5));
                gar6.setVisibility(View.VISIBLE);
                gar6.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(5).getSrc()));
            case 5:
                Log.d("num", "5");
                guideGar.put(R.id.gar5,garbageCloudGenerated.get(4));
                gar5.setVisibility(View.VISIBLE);
                gar5.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(4).getSrc()));
            case 4:

                switch (levelModel.getLevel()){
                    case 2:
                        guideGar.put(R.id.g2b1,garbageCloudGenerated.get(0));
                        g2b1.setVisibility(View.VISIBLE);
                        g2b1.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(0).getSrc()));
                        guideGar.put(R.id.g2b2,garbageCloudGenerated.get(1));
                        g2b2.setVisibility(View.VISIBLE);
                        g2b2.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(1).getSrc()));
                        guideGar.put(R.id.g2b3,garbageCloudGenerated.get(2));
                        g2b3.setVisibility(View.VISIBLE);
                        g2b3.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(2).getSrc()));
                        guideGar.put(R.id.g2b4,garbageCloudGenerated.get(3));
                        g2b4.setVisibility(View.VISIBLE);
                        g2b4.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(3).getSrc()));
                        break;
                    case 3:
                        guideGar.put(R.id.g3b1,garbageCloudGenerated.get(0));
                        g3b1.setVisibility(View.VISIBLE);
                        g3b1.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(0).getSrc()));
                        guideGar.put(R.id.g3b2,garbageCloudGenerated.get(1));
                        g3b2.setVisibility(View.VISIBLE);
                        g3b2.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(1).getSrc()));
                        guideGar.put(R.id.g3b3,garbageCloudGenerated.get(2));
                        g3b3.setVisibility(View.VISIBLE);
                        g3b3.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(2).getSrc()));
                        guideGar.put(R.id.g3b4,garbageCloudGenerated.get(3));
                        g3b4.setVisibility(View.VISIBLE);
                        g3b4.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(3).getSrc()));
                        break;
                    default:
                        Log.d("num", "4");
                        guideGar.put(R.id.gar4,garbageCloudGenerated.get(3));
                        gar4.setVisibility(View.VISIBLE);
                        gar4.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(3).getSrc()));
                }
            case 3:
                Log.d("num", "3-------");
                guideGar.put(R.id.gar1,garbageCloudGenerated.get(0));
                gar1.setVisibility(View.VISIBLE);
                gar1.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(0).getSrc()));
                guideGar.put(R.id.gar2,garbageCloudGenerated.get(1));
                gar2.setVisibility(View.VISIBLE);
                gar2.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(1).getSrc()));
                guideGar.put(R.id.gar3,garbageCloudGenerated.get(2));
                gar3.setVisibility(View.VISIBLE);
                gar3.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(2).getSrc()));
                break;
            case 2:
                guideGar.put(R.id.g1b1,garbageCloudGenerated.get(0));
                g1b1.setVisibility(View.VISIBLE);
                g1b1.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(0).getSrc()));
                guideGar.put(R.id.g1b2,garbageCloudGenerated.get(1));
                g1b2.setVisibility(View.VISIBLE);
                g1b2.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(1).getSrc()));
                break;
            default:
                Log.d("num", "default");
        }
        Singleton.getInstance().setGuideGarbage(guideGar);
    }

    public void timesUp() {
        if(!((Activity) getContext()).isFinishing()) {
            DialogFactory.createColoredDialog(getContext(), "Failed!", new DialogFactory.DialogButtonOnClickListener() {
                @Override
                public void onPositiveButtonClick() {
                    if(isRunning) {
                        isRunning = false;
                        downTimer.cancel();
                    }
                    finish();
                }
            }).show();
        }
    }

    public void setUpGame() {
        Utils.setGrade(getContext(), levelModel.getLevel(),imgLevel, levelBg);
        setGameView(levelModel.getLevel());
        if(levelModel.isWithTime()){
            Singleton.getInstance().setStopTime(false);
            downTimer = new CountDownTimer(levelModel.getTimeLimit(), 1000) {
                public void onTick(long millisUntilFinished) {
                    isRunning = true;
                    tvTime.setText("TIME: " + millisUntilFinished / 1000);
                }
                public void onFinish() {
                    isRunning = false;
                    if(!Singleton.getInstance().isStopTime()) {
                        timesUp();
                    }
                }

            };
            downTimer.start();
        }
    }

    public void setGameView(int grade) {
        if(levelModel.getLevel() != 1 && levelModel.getLevel() != 2 && levelModel.getLevel() != 3) {
            garbagess.setVisibility(View.VISIBLE);
        }

        if(levelModel.isShowEnergy()) {
            tvEnergy.setVisibility(View.VISIBLE);
            tvEnergy.setText("ENERGY: " + levelModel.getEnergy());
        } else {
            tvEnergy.setVisibility(View.GONE);
        }
        switch (grade) {
            case Constant.LEVEL_1:
                llGrade1.setVisibility(View.VISIBLE);
                presenter.generate(Constant.GARBAGE_ALL,2);
                break;
            case Constant.LEVEL_2:
                llGrade1.setVisibility(View.GONE);
                llGrade2.setVisibility(View.VISIBLE);
                presenter.generate(Constant.GARBAGE_ALL,4);
                break;
            case Constant.LEVEL_3:
                llGrade2.setVisibility(View.GONE);
                llGrade3.setVisibility(View.VISIBLE);
                tvTime.setVisibility(View.VISIBLE);
                presenter.generate(Constant.GARBAGE_ALL,4);
                break;
            case Constant.LEVEL_4:
                tvTime.setVisibility(View.GONE);
                llClouds.setVisibility(View.VISIBLE);
                presenter.generateCloud(3);
                break;
            case Constant.LEVEL_5:
                llClouds.setVisibility(View.VISIBLE);
                tvTime.setVisibility(View.VISIBLE);
                presenter.generateCloud(5);
                break;
            case Constant.LEVEL_6:
                llClouds.setVisibility(View.VISIBLE);
                tvTime.setVisibility(View.VISIBLE);
                presenter.generateCloud(8);
                break;
            case Constant.LEVEL_7:
                presenter.generate(Constant.GARBAGE_ALL,4);
                break;
            case Constant.LEVEL_8:
                tvTime.setVisibility(View.VISIBLE);
                presenter.generate(Constant.GARBAGE_ALL,4);
                break;
            case Constant.LEVEL_9:
                presenter.generate(Constant.GARBAGE_BIO,4);
                break;
            case Constant.LEVEL_10:
                tvTime.setVisibility(View.VISIBLE);
                presenter.generate(Constant.GARBAGE_NON_BIO,4);
                break;
        }
    }

    public void setUpRules(int lvl) {
        switch (lvl) {
            case 1:
                insPresenter.getInstruction(1);
            case 2:
                MyShared.setLevel2(getContext(), true);
                Singleton.getInstance().setLevel2(true);
                levelModel.setGarbageType(Constant.GARBAGE_ALL);
                break;
            case 3:
                Singleton.getInstance().setLevel3(true);
                MyShared.setLevel3(getContext(), true);
                levelModel.setGarbageType(Constant.GARBAGE_ALL);
                levelModel.setWithTime(true);
                levelModel.setTimeLimit(50000);
                insPresenter.getInstruction(3);
                break;
            case 4:
                MyShared.setLevel4(getContext(), true);
                Singleton.getInstance().setLevel4(true);
                levelModel.setGarbageType(Constant.GARBAGE_ALL);
                levelModel.setWithTime(false);
                levelModel.setShowCloud(true);
                insPresenter.getInstruction(4);
                break;
            case 5:
                MyShared.setLevel5(getContext(), true);
                Singleton.getInstance().setLevel5(true);
                levelModel.setGarbageType(Constant.GARBAGE_ALL);
                levelModel.setWithTime(true);
                levelModel.setTimeLimit(50000);
                levelModel.setShowCloud(true);
                break;
            case 6:
                MyShared.setLevel6(getContext(), true);
                Singleton.getInstance().setLevel6(true);
                levelModel.setGarbageType(Constant.GARBAGE_ALL);
                levelModel.setWithTime(true);
                levelModel.setShowCloud(true);
                levelModel.setTimeLimit(25000);//9000
                break;
            case 7:
                MyShared.setLevel7(getContext(), true);
                Singleton.getInstance().setLevel7(true);
                levelModel.setGarbageType(Constant.GARBAGE_ALL);
                levelModel.setWithTime(false);
                levelModel.setShowCloud(false);
                insPresenter.getInstruction(7);
                break;
            case 8:
                MyShared.setLevel8(getContext(), true);
                Singleton.getInstance().setLevel8(true);
                levelModel.setGarbageType(Constant.GARBAGE_ALL);
                levelModel.setWithTime(true);
                break;
            case 9:
                MyShared.setLevel9(getContext(), true);
                Singleton.getInstance().setLevel9(true);
                levelModel.setGarbageType(Constant.GARBAGE_BIO);
                levelModel.setWithTime(false);
                levelModel.setShowEnergy(true);
                levelModel.setEnergy(MyShared.getEnergy(this));
                insPresenter.getInstruction(9);
                break;
            case 10:
                MyShared.setLevel10(getContext(), true);
                Singleton.getInstance().setLevel10(true);
                levelModel.setGarbageType(Constant.GARBAGE_NON_BIO);
                levelModel.setWithTime(true);
                levelModel.setTimeLimit(25000);//11000
                levelModel.setShowEnergy(true);
                levelModel.setEnergy(MyShared.getEnergy(this));
                insPresenter.getInstruction(10);
                break;
            case 11:
                MyShared.setLevel11(getContext(), true);
                levelModel.setGarbageType(Constant.GARBAGE_ALL);
                levelModel.setWithTime(true);
                levelModel.setShowEnergy(true);
                levelModel.setEnergy(MyShared.getEnergy(this));
//                levelModel.setTimeLimit(25000); //none
                break;
            case 16:
                levelModel.setGarbageType(Constant.GARBAGE_ALL);
                levelModel.setWithTime(true);
                levelModel.setWithSquare(true);
                levelModel.setTimeLimit(25000);
                levelModel.setTimeGarbageDisappear(5000);
                levelModel.setGarbageCount(5);
                break;
        }
    }

    public void showDialogConfirm() {
        Singleton.getInstance().setStopTime(true);
        DialogFactory.createColoredDialog(getContext(), "Success!!", new DialogFactory.DialogButtonOnClickListener() {
            @Override
            public void onPositiveButtonClick() {
                presenterTrivia.generateTrivia(1);
            }
        }).show();
    }

    public void displayNextLevel() {
        final int lvl = levelModel.getLevel() + 1;
        levelModel = new LevelModel();
        levelModel.setLevel(lvl);
        if(isRunning) {
            isRunning = false;
            downTimer.cancel();
        }
        setUpRules(levelModel.getLevel());
        DialogFactory.createColoredDialog(getContext(), "Level " + lvl, new DialogFactory.DialogButtonOnClickListener() {
            @Override
            public void onPositiveButtonClick() {

                Intent intent;
                if(levelModel.getLevel() > 10) {
                    intent = new Intent(Garbage10Activity.this, Garbage20Activity.class);
                } else {
                    intent = new Intent(Garbage10Activity.this, Garbage10Activity.class);
                }
                intent.putExtra(LEVEL_INFO, Parcels.wrap(levelModel));
                startActivity(intent);
                finish();

            }
        }).show();
    }

    @OnClick(R.id.img_home)
    public void backHome() {
        if(isRunning) {
            isRunning = false;
            downTimer.cancel();
        }
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }


    @Override
    public void processDrop(int garbageType, View v, ViewGroup vg) {
        Log.d("CAN", "" + Utils.getGType(garbageType));
        switch (levelModel.getLevel()) {
            case 1:
                Map<Integer, GarbageModel> itemsCheck0 = Singleton.getInstance().getGuideGarbage();
                if(itemsCheck0.containsKey(v.getId())) {
                    GarbageModel gm = itemsCheck0.get(v.getId());
                    Log.d("TYPE", "" + gm.getType());
                    if(!gm.getType().equalsIgnoreCase(Utils.getGType(garbageType))) {
                        vg.addView(v);
                        new TaskFailed(this).execute();
                    } else {
                        new TaskSuccess(this).execute();
                        g1DropCount = g1DropCount + 1;
                    }
                } else {
                    new TaskFailed(this).execute();
                    vg.addView(v);
                }
                if(g1DropCount == 2) {
                    Singleton.getInstance().setLevel1(true);
                    showDialogConfirm();
                }
                break;
            case 2:
                Map<Integer, GarbageModel> itemsCheck3 = Singleton.getInstance().getGuideGarbage();
                Log.d("DROP_ID", "" + v.getId());
                if(itemsCheck3.containsKey(v.getId())) {
                    GarbageModel gm = itemsCheck3.get(v.getId());
                    if(!gm.getType().equalsIgnoreCase(Utils.getGType(garbageType))) {
                        new TaskFailed(this).execute();
                        vg.addView(v);
                    } else {
                        new TaskSuccess(this).execute();
                        g2DropCount = g2DropCount + 1;
                    }
                } else {
                    new TaskFailed(this).execute();
                    vg.addView(v);
                }
                if(g2DropCount == 4) {
//                    Singleton.getInstance().setLevel2(true);
                    showDialogConfirm();
                }
                break;
            case 3:
                Map<Integer, GarbageModel> itemsCheck2 = Singleton.getInstance().getGuideGarbage();
                Log.d("DROP_ID", "" + v.getId());
                if(itemsCheck2.containsKey(v.getId())) {
                    GarbageModel gm = itemsCheck2.get(v.getId());
                    if(!gm.getType().equalsIgnoreCase(Utils.getGType(garbageType))) {
                        new TaskFailed(this).execute();
                        vg.addView(v);
                    } else {
                        new TaskSuccess(this).execute();
                        g3DropCount = g3DropCount + 1;
                    }
                } else {
                    new TaskFailed(this).execute();
                    vg.addView(v);
                }
                if(g3DropCount == 4) {
//                    Singleton.getInstance().setLevel3(true);
                    showDialogConfirm();
                }
                break;
            case 4:
                Map<Integer, GarbageModel> itemsCheck = Singleton.getInstance().getGuideGarbage();
                Log.d("DROP_ID", "" + v.getId());
                if(itemsCheck.containsKey(v.getId())) {
                    GarbageModel gm = itemsCheck.get(v.getId());
                    if(!gm.getType().equalsIgnoreCase(Utils.getGType(garbageType))) {
                        new TaskFailed(this).execute();
                        vg.addView(v);
                    } else {
                        new TaskSuccess(this).execute();
                        g4DropCount = g4DropCount + 1;
                    }
                } else {
                    new TaskFailed(this).execute();
                    vg.addView(v);
                }
                if(g4DropCount == 3) {
//                    Singleton.getInstance().setLevel4(true);
                    Singleton.getInstance().setGuideGarbage(null);
                    showDialogConfirm();
                }
                break;
            case 5:
                Map<Integer, GarbageModel> items5 = Singleton.getInstance().getGuideGarbage();
                if(items5.containsKey(v.getId())) {
                    GarbageModel gm = items5.get(v.getId());
                    if(!gm.getType().equalsIgnoreCase(Utils.getGType(garbageType))) {
                        new TaskFailed(this).execute();
                        vg.addView(v);
                    } else {
                        new TaskSuccess(this).execute();
                        g5DropCount = g5DropCount + 1;
                    }
                } else {
                    new TaskFailed(this).execute();
                    vg.addView(v);
                }
                if(g5DropCount == 5) {
//                    Singleton.getInstance().setLevel5(true);
                    Singleton.getInstance().setGuideGarbage(null);
                    showDialogConfirm();
                }
                break;
            case 6:
                Map<Integer, GarbageModel> items6 = Singleton.getInstance().getGuideGarbage();
                if(items6.containsKey(v.getId())) {
                    GarbageModel gm = items6.get(v.getId());
                    if(!gm.getType().equalsIgnoreCase(Utils.getGType(garbageType))) {
                        new TaskFailed(this).execute();
                        vg.addView(v);
                    } else {
                        new TaskSuccess(this).execute();
                        g6DropCount = g6DropCount + 1;
                    }
                } else {
                    new TaskFailed(this).execute();
                    vg.addView(v);
                }
                if(g6DropCount == 8) {
//                    Singleton.getInstance().setLevel6(true);
                    showDialogConfirm();
                }
                break;
            case 7:
                Map<Integer, GarbageModel> items7 = Singleton.getInstance().getGuideGarbage();
                GarbageModel gm4 = items7.get(v.getId());
                if(gm4 != null && !gm4.getType().equalsIgnoreCase(Utils.getGType(garbageType))) {
                    new TaskFailed(this).execute();
                    vg.addView(v);
                } else {
                    new TaskSuccess(this).execute();
                    g7DropCount = g7DropCount + 1;
                }
                if(g7DropCount == 4) {
//                    Singleton.getInstance().setLevel7(true);
                    showDialogConfirm();
                }
                break;
            case 8:
                Map<Integer, GarbageModel> items8 = Singleton.getInstance().getGuideGarbage();
                GarbageModel gm3 = items8.get(v.getId());
                if(gm3 != null && !gm3.getType().equalsIgnoreCase(Utils.getGType(garbageType))) {
                    new TaskFailed(this).execute();
                    vg.addView(v);
                } else {
                    new TaskSuccess(this).execute();
                    g8DropCount = g8DropCount + 1;
                }
                if(g8DropCount == 4) {
//                    Singleton.getInstance().setLevel8(true);
                    showDialogConfirm();
                }
                break;
            case 9:
                Map<Integer, GarbageModel> items9 = Singleton.getInstance().getGuideGarbage();
                GarbageModel gm9 = items9.get(v.getId());
                String gtype = Utils.getGType(garbageType);
                if(gtype.equalsIgnoreCase(Constant.GARBAGE_BIO) &&
                        gm9.getType().equalsIgnoreCase(Constant.GARBAGE_BIO)) {
                    new TaskSuccess(this).execute();
                    g9DropCount = g9DropCount + 1;
                } else {
                    new TaskFailed(this).execute();
                    vg.addView(v);
                    int energy = MyShared.getEnergy(this) - 1;
                    if(energy > 0) {
                        MyShared.setEnergy(this, energy);
                        tvEnergy.setText("ENERGY: " + energy);
                    } else {
                        MyShared.setEnergy(this, 0);
                        Intent intent = new Intent(Garbage10Activity.this, MainActivity.class);
                        startActivity(intent);
                    }
                }
                if(g9DropCount == 4) {
//                    Singleton.getInstance().setLevel9(true);
                    showDialogConfirm();
                }
                break;
            case 10:
                Map<Integer, GarbageModel> items10 = Singleton.getInstance().getGuideGarbage();
                GarbageModel gm10 = items10.get(v.getId());
                String gtype1 = Utils.getGType(garbageType);
                if(gtype1.equalsIgnoreCase(Constant.GARBAGE_NON_BIO) &&
                        gm10.getType().equalsIgnoreCase(Constant.GARBAGE_NON_BIO)) {
                    new TaskSuccess(this).execute();
                    g10DropCount = g10DropCount + 1;
                } else {
                    new TaskFailed(this).execute();
                    vg.addView(v);
                    int energy = MyShared.getEnergy(this) - 1;
                    if(energy > 0) {
                        MyShared.setEnergy(this, energy);
                        tvEnergy.setText("ENERGY: " + energy);
                    } else {
                        MyShared.setEnergy(this, 0);
                        Intent intent = new Intent(Garbage10Activity.this, MainActivity.class);
                        startActivity(intent);
                    }
                }
                if(g10DropCount == 4){
//                    Singleton.getInstance().setLevel10(true);
                    showDialogConfirm();
                }
                break;
        }
    }

    @Override
    public void onPause(){
        super.onPause();
        //uncomment this
    }

    @Override
    public void showTrivia(TriviaModel triviaModel) {
        DialogFactory.createTriviaDialog(getContext(), triviaModel.getSrc(), new DialogFactory.DialogButtonOnClickListener() {
            @Override
            public void onPositiveButtonClick() {
                displayNextLevel();
            }
        }).show();
    }

    @Override
    public void showInstruction(int drawableId) {
        Intent intent = new Intent(Garbage10Activity.this, InstructionActivity.class);
        intent.putExtra(InstructionActivity.DID, drawableId);
        startActivity(intent);
    }
}
