package school.wastemanagement.view.level;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.parceler.Parcels;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import butterknife.BindView;
import butterknife.OnClick;
import school.wastemanagement.R;
import school.wastemanagement.base.BaseActivity;
import school.wastemanagement.base.Constant;
import school.wastemanagement.base.DialogFactory;
import school.wastemanagement.base.ForegroundImageView;
import school.wastemanagement.base.MyDragListener;
import school.wastemanagement.base.MyShared;
import school.wastemanagement.base.MyTouchListener;
import school.wastemanagement.base.Singleton;
import school.wastemanagement.base.TaskFailed;
import school.wastemanagement.base.TaskSuccess;
import school.wastemanagement.base.Utils;
import school.wastemanagement.model.GarbageModel;
import school.wastemanagement.model.LevelModel;
import school.wastemanagement.presenter.GarbageContract;
import school.wastemanagement.presenter.GarbagePresenter;
import school.wastemanagement.presenter.InstructionContract;
import school.wastemanagement.presenter.InstructionPresenter;
import school.wastemanagement.view.InstructionActivity;
import school.wastemanagement.view.MainActivity;

import static school.wastemanagement.view.MainActivity.IS_LIST;
import static school.wastemanagement.view.MainActivity.LEVEL_INFO;

public class Garbage20Activity extends BaseActivity implements GarbageContract.View,
        MyDragListener.DropListener,
        MyTouchListener.RemoveGrass,
        InstructionContract.View {

    LevelModel levelModel;
    GarbageContract.Presenter presenter;
    InstructionContract.Presenter insPresenter;
    MyDragListener dragListener;
    MyTouchListener myTouchListener;
    int cloudItems = 0;
    CountDownTimer downTimer;
    boolean isRunning = false;
    boolean isLevel11 = false;
    boolean isList = false;
    List<GarbageModel> level11Garbage = new ArrayList<>();

    @BindView(R.id.img_level) ImageView imgLevel;
    @BindView(R.id.tv_time) TextView tvTime;
    @BindView(R.id.tv_energy) TextView tvEnergy;
    @BindView(R.id.level_bg) LinearLayout levelBg;
    @BindView(R.id.ll_clouds) LinearLayout llClouds;
    @BindView(R.id.garbage) LinearLayout garbagess;
    @BindView(R.id.garLine1) LinearLayout garLine1;
    @BindView(R.id.garLine2) LinearLayout garLine2;
    @BindView(R.id.garLine3) LinearLayout garLine3;
    @BindView(R.id.c1) ImageView cloud1;
    @BindView(R.id.c2) ImageView cloud2;
    @BindView(R.id.c3) ImageView cloud3;
    @BindView(R.id.c4) ImageView cloud4;
    @BindView(R.id.c5) ImageView cloud5;
    @BindView(R.id.c6) ImageView cloud6;
    @BindView(R.id.c7) ImageView cloud7;
    @BindView(R.id.c8) ImageView cloud8;

    //garbage
    @BindView(R.id.img_nonbio) LinearLayout nonBio;
    @BindView(R.id.img_bio) LinearLayout bio;
    @BindView(R.id.img_recycle) LinearLayout recycle;

    //garbage 20
    @BindView(R.id.gar1) ForegroundImageView gar1;
    @BindView(R.id.gar2) ForegroundImageView gar2;
    @BindView(R.id.gar3) ForegroundImageView gar3;
    @BindView(R.id.gar4) ForegroundImageView gar4;
    @BindView(R.id.gar5) ForegroundImageView gar5;
    @BindView(R.id.gar6) ForegroundImageView gar6;
    @BindView(R.id.gar7) ForegroundImageView gar7;
    @BindView(R.id.gar8) ForegroundImageView gar8;
    @BindView(R.id.gar9) ForegroundImageView gar9;
    @BindView(R.id.gar10) ForegroundImageView gar10;
    @BindView(R.id.gar11) ForegroundImageView gar11;
    @BindView(R.id.gar12) ForegroundImageView gar12;
    @BindView(R.id.gar13) ForegroundImageView gar13;
    @BindView(R.id.gar14) ForegroundImageView gar14;
    @BindView(R.id.gar15) ForegroundImageView gar15;
    @BindView(R.id.gar16) ForegroundImageView gar16;
    @BindView(R.id.gar17) ForegroundImageView gar17;
    @BindView(R.id.gar18) ForegroundImageView gar18;
    @BindView(R.id.gar19) ForegroundImageView gar19;
    @BindView(R.id.gar20) ForegroundImageView gar20;

    int g11bio = 0;
    int g11nonbio = 0;
    int g11rec = 0;

    int g12ChkInc = 0;

    int g11DropCount = 0;
    int g12DropCount = 0;
    int g13DropCount = 0;
    int g14DropCount = 0;
    int g15DropCount = 0;
    int g16DropCount = 0;
    int g17DropCount = 0;
    int g18DropCount = 0;
    int g19DropCount = 0;
    int g20DropCount = 0;

    @Override
    protected void loadLayout() {
        levelModel = Parcels.unwrap(getIntent().getParcelableExtra(LEVEL_INFO));
        setContentView(R.layout.activity_garbage20);
        isList = getIntent().getBooleanExtra(IS_LIST, false);
    }

    @Override
    protected void loadPresenter() {
        presenter = new GarbagePresenter(this);
        insPresenter = new InstructionPresenter(this);
    }

    @Override
    protected void loadData() {
        MyShared.getSharedPreferences(this);
        dragListener = new MyDragListener();
        dragListener.setDropListener(this);
        myTouchListener = new MyTouchListener();
        myTouchListener.setRemoveGrass(this);
        nonBio.setOnDragListener(dragListener);
        bio.setOnDragListener(dragListener);
        recycle.setOnDragListener(dragListener);
        gar1.setOnTouchListener(myTouchListener);
        gar2.setOnTouchListener(myTouchListener);
        gar3.setOnTouchListener(myTouchListener);
        gar4.setOnTouchListener(myTouchListener);
        gar5.setOnTouchListener(myTouchListener);
        gar6.setOnTouchListener(myTouchListener);
        gar7.setOnTouchListener(myTouchListener);
        gar8.setOnTouchListener(myTouchListener);
        gar9.setOnTouchListener(myTouchListener);
        gar10.setOnTouchListener(myTouchListener);
        gar11.setOnTouchListener(myTouchListener);
        gar12.setOnTouchListener(myTouchListener);
        gar13.setOnTouchListener(myTouchListener);
        gar14.setOnTouchListener(myTouchListener);
        gar15.setOnTouchListener(myTouchListener);
        gar16.setOnTouchListener(myTouchListener);
        gar17.setOnTouchListener(myTouchListener);
        gar18.setOnTouchListener(myTouchListener);
        gar19.setOnTouchListener(myTouchListener);
        gar20.setOnTouchListener(myTouchListener);
        setUpGame();
        if(isList) {
//            Toast.makeText(this, "list", Toast.LENGTH_SHORT).show();
            setUpRules(levelModel.getLevel());
        }
    }

    @OnClick(R.id.img_home)
    public void backHome() {
        if(isRunning) {
            isRunning = false;
            downTimer.cancel();
        }
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
        finish();
    }

    public void setUpGame() {
        Utils.setGrade(getContext(), levelModel.getLevel(),imgLevel, levelBg);
        setGameView(levelModel.getLevel());
        if(levelModel.isWithTime()){
            Singleton.getInstance().setStopTime(false);
            downTimer = new CountDownTimer(levelModel.getTimeLimit(), 1000) {
                public void onTick(long millisUntilFinished) {
                    isRunning = true;
                    tvTime.setText("TIME: " + millisUntilFinished / 1000);
                }
                public void onFinish() {
                    isRunning = false;
                    if(!Singleton.getInstance().isStopTime()) {
                        timesUp();
                    }
                }

            };
            downTimer.start();
        }
    }

    public void setUpRules(int lvl) {
        switch (lvl) {
            case 11:
                MyShared.setLevel11(getContext(), true);
                Singleton.getInstance().setLevel11(true);
                levelModel.setGarbageType(Constant.GARBAGE_ALL);
                levelModel.setWithTime(true);
                levelModel.setShowEnergy(true);
                levelModel.setEnergy(MyShared.getEnergy(this));
                insPresenter.getInstruction(11);
                break;
            case 12:
                MyShared.setLevel12(getContext(), true);
                Singleton.getInstance().setLevel12(true);
                levelModel.setGarbageType(Constant.GARBAGE_ALL);
                levelModel.setShowCloud(true);
                levelModel.setGarbageCount(6);
                levelModel.setTimeGarbageDisappear(10000);//10000
                levelModel.setAnyOrder(true);
                levelModel.setShowEnergy(true);
                levelModel.setEnergy(MyShared.getEnergy(this));
                insPresenter.getInstruction(12);

                break;
            case 13:
                MyShared.setLevel13(getContext(), true);
                Singleton.getInstance().setLevel13(true);
                levelModel.setGarbageType(Constant.GARBAGE_ALL);
                levelModel.setShowCloud(true);
                levelModel.setGarbageCount(6);
                levelModel.setTimeGarbageDisappear(10000);//10000
                levelModel.setAnyOrder(false);
                levelModel.setShowEnergy(true);
                levelModel.setEnergy(MyShared.getEnergy(this));
                insPresenter.getInstruction(13);
                break;
            case 14:
                MyShared.setLevel14(getContext(), true);
                Singleton.getInstance().setLevel14(true);
                levelModel.setGarbageType(Constant.GARBAGE_ALL);
                levelModel.setShowCloud(true);
                levelModel.setGarbageCount(5);
                levelModel.setWithGrass(true);
                levelModel.setShowEnergy(true);
                levelModel.setEnergy(MyShared.getEnergy(this));
                insPresenter.getInstruction(14);
                break;
            case 15:
                MyShared.setLevel15(getContext(), true);
                Singleton.getInstance().setLevel15(true);
                levelModel.setGarbageType(Constant.GARBAGE_ALL);
                levelModel.setShowCloud(true);
                levelModel.setWithTime(true);
                levelModel.setGarbageCount(7);
                levelModel.setWithGrass(true);
                levelModel.setTimeLimit(25000);//10000
                levelModel.setShowEnergy(true);
                levelModel.setEnergy(MyShared.getEnergy(this));
                break;
            case 16:
                MyShared.setLevel16(getContext(), true);
                Singleton.getInstance().setLevel16(true);
                levelModel.setGarbageType(Constant.GARBAGE_ALL);
                levelModel.setWithTime(true);
                levelModel.setWithSquare(true);
                levelModel.setTimeLimit(25000);
                levelModel.setTimeGarbageDisappear(5000);
                levelModel.setGarbageCount(5);
                levelModel.setShowEnergy(true);
                levelModel.setEnergy(MyShared.getEnergy(this));
                insPresenter.getInstruction(16);
                break;
            case 17:
                MyShared.setLevel17(getContext(), true);
                Singleton.getInstance().setLevel17(true);
                levelModel.setGarbageType(Constant.GARBAGE_ALL);
                levelModel.setWithTime(true);
                levelModel.setTimeLimit(25000);//17000
                levelModel.setGarbageCount(10);
                levelModel.setShowEnergy(true);
                levelModel.setEnergy(MyShared.getEnergy(this));
                insPresenter.getInstruction(17);
                break;
            case 18:
                MyShared.setLevel18(getContext(), true);
                Singleton.getInstance().setLevel18(true);
                levelModel.setGarbageType(Constant.GARBAGE_ALL);
                levelModel.setWithTime(true);
                levelModel.setTimeLimit(25000);//14000
                levelModel.setGarbageCount(13);
                levelModel.setShowEnergy(true);
                levelModel.setEnergy(MyShared.getEnergy(this));
                insPresenter.getInstruction(18);
                break;
            case 19:
                MyShared.setLevel19(getContext(), true);
                Singleton.getInstance().setLevel19(true);
                levelModel.setGarbageType(Constant.GARBAGE_ALL);
                levelModel.setWithTime(true);
                levelModel.setTimeLimit(25000);//10000
                levelModel.setGarbageCount(16);
                levelModel.setShowEnergy(true);
                levelModel.setEnergy(MyShared.getEnergy(this));
                insPresenter.getInstruction(19);
                break;
            case 20:
                MyShared.setLevel20(getContext(), true);
                Singleton.getInstance().setLevel20(true);
                levelModel.setGarbageType(Constant.GARBAGE_ALL);
                levelModel.setShowEnergy(true);
                levelModel.setEnergy(MyShared.getEnergy(this));
                levelModel.setGarbageCount(20);
                break;
        }
    }

    public void setGameView(int grade) {
        garbagess.setVisibility(View.VISIBLE);
        if(levelModel.isWithTime()) {
            tvTime.setVisibility(View.VISIBLE);
        }
        if(levelModel.isShowCloud()) {
            llClouds.setVisibility(View.VISIBLE);
        }
        if(levelModel.isShowEnergy()) {
            tvEnergy.setVisibility(View.VISIBLE);
            tvEnergy.setText("ENERGY: " + levelModel.getEnergy());
        } else {
            tvEnergy.setVisibility(View.GONE);
        }
        switch (grade) {
            case Constant.LEVEL_11:
                isLevel11 = true;
                presenter.generate(Constant.GARBAGE_BIO,3);
                break;
            case Constant.LEVEL_12:
                presenter.generateCloud(levelModel.getGarbageCount());
                break;
            case Constant.LEVEL_13:
                presenter.generateCloud(levelModel.getGarbageCount());
                break;
            case Constant.LEVEL_14:
                presenter.generateCloud(levelModel.getGarbageCount());
                break;
            case Constant.LEVEL_15:
                presenter.generateCloud(levelModel.getGarbageCount());
                break;
            case Constant.LEVEL_16:
                presenter.generateCloud(levelModel.getGarbageCount());
                break;
            case Constant.LEVEL_17:
                presenter.generate(Constant.GARBAGE_ALL, levelModel.getGarbageCount());
                break;
            case Constant.LEVEL_18:
                presenter.generate(Constant.GARBAGE_ALL, levelModel.getGarbageCount());
                break;
            case Constant.LEVEL_19:
                presenter.generate(Constant.GARBAGE_ALL, levelModel.getGarbageCount());
                break;
            case Constant.LEVEL_20:
                presenter.generate(Constant.GARBAGE_ALL, levelModel.getGarbageCount());
                break;
        }
    }

    @Override
    public void garbageList(List<GarbageModel> garbageGenerated) {
        int diff = 20 - levelModel.getGarbageCount();
        switch (levelModel.getLevel()){
            case 20:
            case 19:
            case 18:
            case 17:
                presenter.getCloudPartner(levelModel.getGarbageCount() + diff, garbageGenerated);
                break;
            case 11:
                level11Garbage.addAll(garbageGenerated);
                if(level11Garbage.size() == 3) {
                    presenter.generate(Constant.GARBAGE_NON_BIO, 4);
                } else if(level11Garbage.size() == 7) {
                    presenter.generate(Constant.GARBAGE_REC, 4);
                } else {
                    presenter.getCloudPartner(13, level11Garbage);
                }
                break;
        }
    }

    @Override
    public void cloudGarbageList(final List<GarbageModel> garbageCloudGenerated) {
        cloudItems = garbageCloudGenerated.size();
        Log.d("garbageCloudGenerated", garbageCloudGenerated.size() + " garbage");
        switch (garbageCloudGenerated.size()){
            case 8:
                cloud8.setVisibility(View.VISIBLE);
                cloud8.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(7).getSrc()));
            case 7:
                cloud7.setVisibility(View.VISIBLE);
                cloud7.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(6).getSrc()));
            case 6:
                cloud6.setVisibility(View.VISIBLE);
                cloud6.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(5).getSrc()));
            case 5:
                cloud5.setVisibility(View.VISIBLE);
                cloud5.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(4).getSrc()));
            case 4:
                cloud4.setVisibility(View.VISIBLE);
                cloud4.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(3).getSrc()));
            case 3:
                cloud1.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(0).getSrc()));
                cloud2.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(1).getSrc()));
                cloud3.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(2).getSrc()));
                break;
        }
        switch (levelModel.getLevel()){
            case 16:
            case 15:
            case 14:
                presenter.getCloudPartner(levelModel.getGarbageCount() + 4, garbageCloudGenerated);
                break;
            case 13:
                tvTime.setVisibility(View.VISIBLE);
                new CountDownTimer(levelModel.getTimeGarbageDisappear(), 1000) {
                    public void onTick(long millisUntilFinished) {
                        tvTime.setText("TIME: " + millisUntilFinished / 1000);
                    }
                    public void onFinish() {
                        llClouds.setVisibility(View.GONE);
                        tvTime.setVisibility(View.GONE);
                        presenter.getCloudPartner(levelModel.getGarbageCount() + 4, garbageCloudGenerated);
                    }
                }.start();
                break;
            case 12:
                tvTime.setVisibility(View.VISIBLE);
                new CountDownTimer(levelModel.getTimeGarbageDisappear(), 1000) {
                    public void onTick(long millisUntilFinished) {
                        tvTime.setText("TIME: " + millisUntilFinished / 1000);
                    }
                    public void onFinish() {
                        llClouds.setVisibility(View.GONE);
                        tvTime.setVisibility(View.GONE);
                        presenter.getCloudPartner(levelModel.getGarbageCount() + 4, garbageCloudGenerated);
                    }

                }.start();
                break;
        }
    }

    public void showDialogConfirm() {
        final int lvl = levelModel.getLevel() + 1;
        Singleton.getInstance().setStopTime(true);
        levelModel = new LevelModel();
        levelModel.setLevel(lvl);
        DialogFactory.createColoredDialog(getContext(), "Success!!", new DialogFactory.DialogButtonOnClickListener() {
            @Override
            public void onPositiveButtonClick() {
                if(lvl == 21) {
                    DialogFactory.createColoredDialog(getContext(), "Congratulations!" , new DialogFactory.DialogButtonOnClickListener() {
                        @Override
                        public void onPositiveButtonClick() {
                            Intent intent = new Intent(Garbage20Activity.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    }).show();
                }
                setUpRules(levelModel.getLevel());
                DialogFactory.createColoredDialog(getContext(), "Level " + lvl, new DialogFactory.DialogButtonOnClickListener() {
                    @Override
                    public void onPositiveButtonClick() {
                        if(isRunning) {
                            isRunning = false;
                            downTimer.cancel();
                        }

                        Intent intent = new Intent(Garbage20Activity.this, Garbage20Activity.class);
                        intent.putExtra(LEVEL_INFO, Parcels.wrap(levelModel));
                        startActivity(intent);
                        finish();

                    }
                }).show();
            }
        }).show();
    }

    @Override
    public void genWithCloud(List<GarbageModel> garbageCloudGenerated) {
        setUpGarbage(garbageCloudGenerated);
    }

    public void decreaseEnergy() {
        new TaskFailed(this).execute();
        int energy = levelModel.getEnergy() - 1;
        if(energy > 0) {
            levelModel.setEnergy(energy);
            tvEnergy.setText("ENERGY: " + energy);
        } else {
            Singleton.getInstance().setEnergy(energy);
            Intent intent = new Intent(Garbage20Activity.this, MainActivity.class);
            startActivity(intent);
        }
    }

    public void increaseEnergy(){
        new TaskSuccess(this).execute();
//        int energy = levelModel.getEnergy() + 1;
//        levelModel.setEnergy(energy);
//        tvEnergy.setText("ENERGY: " + energy);
    }

    @Override
    public void processDrop(int viewId, View v, ViewGroup vg) {
        switch (levelModel.getLevel()) {
            case 11:
                Map<Integer, GarbageModel> items11 = Singleton.getInstance().getGuideGarbage();
                GarbageModel gm = items11.get(v.getId());
                String gtype1 = Utils.getGType(viewId);
                if(g11bio != 3) {
                    if (gtype1.equalsIgnoreCase(Constant.GARBAGE_BIO) &&
                            gm.getType().equalsIgnoreCase(Constant.GARBAGE_BIO)) {
                        new TaskSuccess(this).execute();
                        g11bio++;
                        increaseEnergy();
                        g11DropCount = g11DropCount + 1;
                    } else {
                        vg.addView(v);
                        decreaseEnergy();
                    }
                } else {
                    if(g11nonbio != 4){
                        if (gtype1.equalsIgnoreCase(Constant.GARBAGE_NON_BIO) &&
                                gm.getType().equalsIgnoreCase(Constant.GARBAGE_NON_BIO)) {
                            new TaskSuccess(this).execute();
                            g11nonbio++;
                            increaseEnergy();
                            g11DropCount = g11DropCount + 1;
                        } else {
                            vg.addView(v);
                            decreaseEnergy();
                        }
                    } else {
                        if(g11rec != 4){
                            if (gtype1.equalsIgnoreCase(Constant.GARBAGE_REC) &&
                                    gm.getType().equalsIgnoreCase(Constant.GARBAGE_REC)) {
                                new TaskSuccess(this).execute();
                                g11rec++;
                                increaseEnergy();
                                g11DropCount = g11DropCount + 1;
                            } else {
                                vg.addView(v);
                                decreaseEnergy();
                            }
                        }
                    }
                }
                if(g11DropCount == 11) {
//                    Singleton.getInstance().setLevel11(true);
                    Singleton.getInstance().setGuideGarbage(null);
                    Singleton.getInstance().setEnergy(levelModel.getEnergy());
                    showDialogConfirm();
                }
                break;
            case 12:
                Map<Integer, GarbageModel> items12 = Singleton.getInstance().getGuideGarbage();
                GarbageModel gm12 = items12.get(v.getId());
                if(gm12 != null && !gm12.getType().equalsIgnoreCase(Utils.getGType(viewId))) {
                    vg.addView(v);
                    decreaseEnergy();
                } else {

                    increaseEnergy();
                    g12DropCount = g12DropCount + 1;
                }
                if(g12DropCount == 6) {
//                    Singleton.getInstance().setLevel12(true);
                    Singleton.getInstance().setGuideGarbage(null);
                    Singleton.getInstance().setEnergy(levelModel.getEnergy());
                    showDialogConfirm();
                }
                break;
            case 13:
                Map<Integer, GarbageModel> items13 = Singleton.getInstance().getGuideGarbage();
                GarbageModel gm13 = items13.get(v.getId());
                if(gm13 != null && !gm13.getType().equalsIgnoreCase(Utils.getGType(viewId))) {
                    vg.addView(v);
                    decreaseEnergy();
                } else {
                    if (g12ChkInc == 0) {
                        g12ChkInc++;
                    }
                    if(gm13.getSort() == g12ChkInc) {
                        increaseEnergy();
                        g13DropCount = g13DropCount + 1;
                        g12ChkInc++;
                    } else {
                        decreaseEnergy();
                        vg.addView(v);
                    }

                    Log.d("drop",gm13.getSrc()+"");

                }
                if(g13DropCount == 6) {
//                    Singleton.getInstance().setLevel13(true);
                    Singleton.getInstance().setGuideGarbage(null);
                    Singleton.getInstance().setEnergy(levelModel.getEnergy());
                    showDialogConfirm();
                }
                break;
            case 14:
                Map<Integer, GarbageModel> items14 = Singleton.getInstance().getGuideGarbage();
                Log.d("DROP_ID", "" + v.getId());
                if(items14.containsKey(v.getId())) {
                    GarbageModel gm14 = items14.get(v.getId());
                    if(!gm14.getType().equalsIgnoreCase(Utils.getGType(viewId))) {
                        vg.addView(v);
                        decreaseEnergy();
                    } else {
                        increaseEnergy();
                        g14DropCount = g14DropCount + 1;
                    }
                } else {
                    vg.addView(v);
                    decreaseEnergy();
                }
                if(g14DropCount == 5) {
//                    Singleton.getInstance().setLevel14(true);
                    Singleton.getInstance().setGuideGarbage(null);
                    Singleton.getInstance().setEnergy(levelModel.getEnergy());
                    showDialogConfirm();
                }
                break;
            case 15:
                Map<Integer, GarbageModel> items15 = Singleton.getInstance().getGuideGarbage();
                Log.d("DROP_ID", "" + v.getId());
                if(items15.containsKey(v.getId())) {
                    GarbageModel gm15 = items15.get(v.getId());
                    if(!gm15.getType().equalsIgnoreCase(Utils.getGType(viewId))) {
                        vg.addView(v);
                        decreaseEnergy();
                    } else {
                        increaseEnergy();
                        g15DropCount = g15DropCount + 1;
                    }
                } else {
                    decreaseEnergy();
                    vg.addView(v);
                }
                if(g15DropCount == 7) {
//                    Singleton.getInstance().setLevel15(true);
                    Singleton.getInstance().setGuideGarbage(null);
                    Singleton.getInstance().setEnergy(levelModel.getEnergy());
                    showDialogConfirm();
                }
                break;
            case 16:
                Map<Integer, GarbageModel> items16 = Singleton.getInstance().getGuideGarbage();
                Log.d("DROP_ID", "" + v.getId());
                if(items16.containsKey(v.getId())) {
                    GarbageModel gm16 = items16.get(v.getId());
                    if(!gm16.getType().equalsIgnoreCase(Utils.getGType(viewId))) {
                        vg.addView(v);
                        decreaseEnergy();
                    } else {
                        increaseEnergy();
                        g16DropCount = g16DropCount + 1;
                    }
                } else {
                    decreaseEnergy();
                    vg.addView(v);
                    if(linearLayoutChildCount(garLine1) != 5){
                        garLine1.addView(v);
                    } else if(linearLayoutChildCount(garLine2) != 5) {
                        garLine2.addView(v);
                    } else if(linearLayoutChildCount(garLine3) != 5) {
                        garLine3.addView(v);
                    }
                }
                if(g16DropCount == 7) {
//                    Singleton.getInstance().setLevel16(true);
                    Singleton.getInstance().setGuideGarbage(null);
                    Singleton.getInstance().setEnergy(levelModel.getEnergy());
                    showDialogConfirm();
                }
                break;
            case 17:
                Map<Integer, GarbageModel> items17 = Singleton.getInstance().getGuideGarbage();
                Log.d("DROP_ID", "" + v.getId());
                GarbageModel gm17 = items17.get(v.getId());
                if(!gm17.getType().equalsIgnoreCase(Utils.getGType(viewId))) {
                    vg.addView(v);
                    decreaseEnergy();
                } else {
                    increaseEnergy();
                    g17DropCount = g17DropCount + 1;
                }
                if(g17DropCount == 10) {
//                    Singleton.getInstance().setLevel17(true);
                    Singleton.getInstance().setGuideGarbage(null);
                    Singleton.getInstance().setEnergy(levelModel.getEnergy());
                    showDialogConfirm();
                }
                break;
            case 18:
                Map<Integer, GarbageModel> items18 = Singleton.getInstance().getGuideGarbage();
                Log.d("DROP_ID", "" + v.getId());
                GarbageModel gm18 = items18.get(v.getId());
                if(gm18 != null && !gm18.getType().equalsIgnoreCase(Utils.getGType(viewId))) {
                    vg.addView(v);
                    decreaseEnergy();
                } else {
                    increaseEnergy();
                    g18DropCount = g18DropCount + 1;
                }
                if(g18DropCount == 13) {
//                    Singleton.getInstance().setLevel18(true);
                    Singleton.getInstance().setGuideGarbage(null);
                    Singleton.getInstance().setEnergy(levelModel.getEnergy());
                    showDialogConfirm();
                }
                break;
            case 19:
                Map<Integer, GarbageModel> items19 = Singleton.getInstance().getGuideGarbage();
                Log.d("DROP_ID", "" + v.getId());
                GarbageModel gm19 = items19.get(v.getId());
                if(gm19 != null && !gm19.getType().equalsIgnoreCase(Utils.getGType(viewId))) {
                    vg.addView(v);
                    decreaseEnergy();
                } else {
                    increaseEnergy();
                    g19DropCount = g19DropCount + 1;
                }
                if(g19DropCount == 16) {
//                    Singleton.getInstance().setLevel19(true);
                    Singleton.getInstance().setGuideGarbage(null);
                    Singleton.getInstance().setEnergy(levelModel.getEnergy());
                    showDialogConfirm();
                }
                break;
            case 20:
                Map<Integer, GarbageModel> items20 = Singleton.getInstance().getGuideGarbage();
                Log.d("DROP_ID", "" + v.getId());
                GarbageModel gm20 = items20.get(v.getId());
                if(gm20 != null && !gm20.getType().equalsIgnoreCase(Utils.getGType(viewId))) {
                    vg.addView(v);
                    decreaseEnergy();
                } else {
                    increaseEnergy();
                    g20DropCount = g20DropCount + 1;
                }
                if(g20DropCount == 20) {
//                    Singleton.getInstance().setLevel20(true);
                    Singleton.getInstance().setGuideGarbage(null);
                    Singleton.getInstance().setEnergy(levelModel.getEnergy());
                    showDialogConfirm();
                }
                break;
        }
    }

    @Override
    public void onPause(){
        super.onPause();
        //uncomment this
    }

    public int linearLayoutChildCount(ViewGroup parent) {
        int count = 0;
        for (int x = 0; x < parent.getChildCount(); x++) {
            if (parent.getChildAt(x) instanceof LinearLayout) {
                count++;
            }
        }
        return count;
    }

    public void timesUp() {
        if(!((Activity) getContext()).isFinishing()) {
            DialogFactory.createColoredDialog(getContext(), "Failed!", new DialogFactory.DialogButtonOnClickListener() {
                @Override
                public void onPositiveButtonClick() {
                    if(isRunning) {
                        isRunning = false;
                        downTimer.cancel();
                    }
                    finish();
                }
            }).show();
        }
    }

    public void setUpGarbage(List<GarbageModel> garbageCloudGenerated) {
        Map<Integer, GarbageModel> guideGar = new HashMap<>();
        cloudItems = garbageCloudGenerated.size();
        Drawable grassId = getResources().getDrawable(R.drawable.iv_grass);
        switch (garbageCloudGenerated.size()) {
            case 20:
                if(cloudItems >= 20) guideGar.put(R.id.gar20,garbageCloudGenerated.get(19));
                if(garbageCloudGenerated.get(19) != null) gar20.setVisibility(View.VISIBLE);
                if (levelModel.isWithGrass()) gar20.setForeground(grassId);
                gar20.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(19).getSrc()));
            case 19:
                if(cloudItems >= 19) guideGar.put(R.id.gar19,garbageCloudGenerated.get(18));
                if(garbageCloudGenerated.get(18) != null) gar19.setVisibility(View.VISIBLE);
                if (levelModel.isWithGrass()) gar19.setForeground(grassId);
                gar19.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(18).getSrc()));
            case 18:
                if(cloudItems >= 18) guideGar.put(R.id.gar18,garbageCloudGenerated.get(17));
                if(garbageCloudGenerated.get(17) != null) gar18.setVisibility(View.VISIBLE);
                if (levelModel.isWithGrass()) gar18.setForeground(grassId);
                gar18.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(17).getSrc()));
            case 17:
                if(cloudItems >= 17) guideGar.put(R.id.gar17,garbageCloudGenerated.get(16));
                if(garbageCloudGenerated.get(16) != null) gar17.setVisibility(View.VISIBLE);
                if (levelModel.isWithGrass()) gar17.setForeground(grassId);
                gar17.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(16).getSrc()));
            case 16:
                if(cloudItems >= 16) guideGar.put(R.id.gar16,garbageCloudGenerated.get(15));
                if(garbageCloudGenerated.get(15) != null) gar16.setVisibility(View.VISIBLE);
                if (levelModel.isWithGrass()) gar16.setForeground(grassId);
                gar16.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(15).getSrc()));
            case 15:
                if(cloudItems >= 15) guideGar.put(R.id.gar15,garbageCloudGenerated.get(14));
                if(garbageCloudGenerated.get(14) != null) gar15.setVisibility(View.VISIBLE);
                if (levelModel.isWithGrass()) gar15.setForeground(grassId);
                gar15.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(14).getSrc()));
            case 14:
                if(cloudItems >= 14) guideGar.put(R.id.gar14,garbageCloudGenerated.get(13));
                if(garbageCloudGenerated.get(13) != null) gar14.setVisibility(View.VISIBLE);
                if (levelModel.isWithGrass()) gar14.setForeground(grassId);
                gar14.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(13).getSrc()));
            case 13:
                if(cloudItems >= 13) guideGar.put(R.id.gar13,garbageCloudGenerated.get(12));
                if(garbageCloudGenerated.get(12) != null) gar13.setVisibility(View.VISIBLE);
                if (levelModel.isWithGrass()) gar13.setForeground(grassId);
                gar13.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(12).getSrc()));
            case 12:
                if(cloudItems >= 12) guideGar.put(R.id.gar12,garbageCloudGenerated.get(11));
                if(garbageCloudGenerated.get(11) != null) gar12.setVisibility(View.VISIBLE);
                if (levelModel.isWithGrass()) gar12.setForeground(grassId);
                gar12.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(11).getSrc()));
            case 11:
                if(cloudItems >= 11) guideGar.put(R.id.gar11,garbageCloudGenerated.get(10));
                if(garbageCloudGenerated.get(10) != null) gar11.setVisibility(View.VISIBLE);
                if (levelModel.isWithGrass()) gar11.setForeground(grassId);
                gar11.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(10).getSrc()));
            case 10:
                if(cloudItems >= 10) guideGar.put(R.id.gar10,garbageCloudGenerated.get(9));
                if(garbageCloudGenerated.get(9) != null) gar10.setVisibility(View.VISIBLE);
                if (levelModel.isWithGrass()) gar10.setForeground(grassId);
                gar10.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(9).getSrc()));
            case 9:
                if(cloudItems >= 9) guideGar.put(R.id.gar9,garbageCloudGenerated.get(8));
                if(garbageCloudGenerated.get(8) != null) gar9.setVisibility(View.VISIBLE);
                if (levelModel.isWithGrass()) gar9.setForeground(grassId);
                gar9.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(8).getSrc()));
            case 8:
                if(cloudItems >= 8)guideGar.put(R.id.gar8,garbageCloudGenerated.get(7));
                if(garbageCloudGenerated.get(7) != null) gar8.setVisibility(View.VISIBLE);
                if (levelModel.isWithGrass()) gar8.setForeground(grassId);
                gar8.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(7).getSrc()));
            case 7:
                if(cloudItems >= 7) guideGar.put(R.id.gar7,garbageCloudGenerated.get(6));
                if(garbageCloudGenerated.get(6) != null) gar7.setVisibility(View.VISIBLE);
                if (levelModel.isWithGrass()) gar7.setForeground(grassId);
                gar7.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(6).getSrc()));
            case 6:
                if(cloudItems >= 6)guideGar.put(R.id.gar6,garbageCloudGenerated.get(5));
                if (levelModel.isWithGrass()) gar6.setForeground(grassId);
                gar6.setVisibility(View.VISIBLE);
                gar6.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(5).getSrc()));
            case 5:
                Log.d("num", "5");
                if(cloudItems >= 5) guideGar.put(R.id.gar5,garbageCloudGenerated.get(4));
                if (levelModel.isWithGrass()) gar5.setForeground(grassId);
                gar5.setVisibility(View.VISIBLE);
                gar5.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(4).getSrc()));
            case 4:
                Log.d("num", "4");
                if(cloudItems >= 4) guideGar.put(R.id.gar4,garbageCloudGenerated.get(3));
                if (levelModel.isWithGrass()) gar4.setForeground(grassId);
                gar4.setVisibility(View.VISIBLE);
                gar4.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(3).getSrc()));
            case 3:
                Log.d("num", "3");
                guideGar.put(R.id.gar1,garbageCloudGenerated.get(0));
                if (levelModel.isWithGrass()){
                    gar1.setForeground(grassId);
                    gar2.setForeground(grassId);
                    gar3.setForeground(grassId);
                }
                gar1.setVisibility(View.VISIBLE);
                gar1.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(0).getSrc()));
                guideGar.put(R.id.gar2,garbageCloudGenerated.get(1));
                gar2.setVisibility(View.VISIBLE);
                gar2.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(1).getSrc()));
                guideGar.put(R.id.gar3,garbageCloudGenerated.get(2));
                gar3.setVisibility(View.VISIBLE);
                gar3.setImageDrawable(getResources().getDrawable(garbageCloudGenerated.get(2).getSrc()));
                break;
            default:
                Log.d("num", "default");
        }

        Singleton.getInstance().setGuideGarbage(guideGar);
    }

    @Override
    public void removeGrass() {

    }

    @Override
    public void showInstruction(int drawableId) {
        Intent intent = new Intent(Garbage20Activity.this, InstructionActivity.class);
        intent.putExtra(InstructionActivity.DID, drawableId);
        startActivity(intent);
    }
}
