package school.wastemanagement.presenter;

import android.graphics.drawable.Drawable;

import school.wastemanagement.base.Base;

public interface InstructionContract {
    interface View extends Base {
        void showInstruction(int drawableId);
    }
    interface Presenter {
        void getInstruction(int level);
    }
}
