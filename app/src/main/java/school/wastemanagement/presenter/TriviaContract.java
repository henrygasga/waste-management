package school.wastemanagement.presenter;

import school.wastemanagement.base.Base;
import school.wastemanagement.model.TriviaModel;

public interface TriviaContract {
    interface View extends Base {
        void showTrivia(TriviaModel triviaModel);
    }
    interface Presenter {
        void generateTrivia(int pieces);
    }
}
