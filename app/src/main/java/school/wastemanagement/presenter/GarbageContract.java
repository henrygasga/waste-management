package school.wastemanagement.presenter;

import java.util.List;

import school.wastemanagement.base.Base;
import school.wastemanagement.model.GarbageModel;

public interface GarbageContract {
    interface View extends Base{
        void garbageList(List<GarbageModel> garbageGenerated);
        void cloudGarbageList(List<GarbageModel> garbageCloudGenerated);
        void genWithCloud(List<GarbageModel> garbageCloudGenerated);
    }
    interface Presenter {
        void generate(String garbageType, int show);
        void generateCloud(int pieces);
        void getCloudPartner(int show, List<GarbageModel> garbageCloudGenerated);
    }
}
