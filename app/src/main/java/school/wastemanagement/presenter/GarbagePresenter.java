package school.wastemanagement.presenter;

import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import school.wastemanagement.base.Constant;
import school.wastemanagement.model.GarbageModel;

public class GarbagePresenter implements GarbageContract.Presenter {

    private GarbageContract.View view;
    private List<GarbageModel> generatedList;
    public static List<GarbageModel> generatedCloudList;

    public GarbagePresenter(GarbageContract.View v) {
        this.view = v;
    }

    @Override
    public void generate(String garbageType, int show) {
        int length = getType(garbageType).length - 1;
        List<GarbageModel> get = new ArrayList<>();
        generatedList = new ArrayList<>();
        Random rand = new Random();

        int willGet = length > show ? show : length;

        int rs;
        int rc;
        do {
            rc = rand.nextInt(3) + 1;

            String tGarnage = garbageType.equalsIgnoreCase(Constant.GARBAGE_ALL) ? getGType(rc) : garbageType;
            rs = rand.nextInt(getType(tGarnage).length - 1);
            GarbageModel gm = new GarbageModel();
            gm.setSrc(getType(tGarnage)[rs]);
            gm.setType(tGarnage);
            if(!generatedList.contains(gm)) {
                generatedList.add(gm);
            }
        } while (generatedList.size() != willGet);

        view.garbageList(generatedList);
    }

    @Override
    public void generateCloud(int pieces) {
        generatedCloudList = new ArrayList<>();
        Random rand = new Random();
        int rs;
        int rc;
        int sort = 1;
        do {
            rc = rand.nextInt(3) + 1;
            rs = rand.nextInt(getType(getGType(rc)).length - 1);
            GarbageModel gm = new GarbageModel();
            gm.setSrc(getType(getGType(rc))[rs]);
            gm.setType(getGType(rc));
            gm.setSort(sort);
            if(!generatedCloudList.contains(gm)) {
                generatedCloudList.add(gm);
                sort++;
            }
        } while (generatedCloudList.size() != pieces);

        view.cloudGarbageList(generatedCloudList);

    }

    @Override
    public void getCloudPartner(int show, List<GarbageModel> garbageCloudGenerated) {
        List<GarbageModel> garbageAll;
        garbageAll = garbageCloudGenerated;
        Random rand = new Random();
        int rs;
        int rc;
        do {
            rc = rand.nextInt(3) + 1;
            rs = rand.nextInt(getType(getGType(rc)).length - 1);
            GarbageModel gm = new GarbageModel();
            gm.setSrc(getType(getGType(rc))[rs]);
            gm.setType(getGType(rc));
            if(!garbageAll.contains(gm)) {
                garbageAll.add(gm);
            }
        } while (garbageAll.size() != show);

        view.genWithCloud(garbageAll);
    }

    public static List<GarbageModel> getGeneratedCloudList() {
        return generatedCloudList;
    }

    public String getGType(int o) {
        switch (o){
            case 1:
                return Constant.GARBAGE_BIO;
            case 2:
                return Constant.GARBAGE_NON_BIO;
            default:
                return Constant.GARBAGE_REC;
        }
    }

    public int[] getType(String type) {
        switch (type) {
            case Constant.GARBAGE_BIO:
                return Constant.B;
            case Constant.GARBAGE_NON_BIO:
                return Constant.NB;
            default:
                return Constant.RB;
        }
    }
}
