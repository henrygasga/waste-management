package school.wastemanagement.presenter;

import android.graphics.drawable.Drawable;

import school.wastemanagement.R;

public class InstructionPresenter implements InstructionContract.Presenter{

    private InstructionContract.View view;

    public InstructionPresenter() {
    }

    public InstructionPresenter(InstructionContract.View view) {
        this.view = view;
    }

    @Override
    public void getInstruction(int level) {
        view.showInstruction(instruction(level));
    }
    public int instruction(int level) {
        switch (level){
            case 1:
                return R.drawable.instruction_1;
            case 3:
                return R.drawable.instruction_3;
            case 4:
                return R.drawable.instruction_4;
            case 7:
                return R.drawable.instruction_7;
            case 9:
                return R.drawable.instruction_9;
            case 10:
                return R.drawable.instruction_10;
            case 11:
                return R.drawable.instruction_11;
            case 12:
                return R.drawable.instruction_12;
            case 13:
                return R.drawable.instruction_13;
            case 14:
                return R.drawable.instruction_14;
            case 16:
                return R.drawable.instruction_16;
            case 17:
                return R.drawable.instruction_17;
            case 18:
                return R.drawable.instruction_18;
            case 19:
                return R.drawable.instruction_19;
            default:
                return 0;
        }
    }
}
