package school.wastemanagement.presenter;

import java.util.Random;

import school.wastemanagement.base.Constant;
import school.wastemanagement.model.TriviaModel;

public class TriviaPresenter implements TriviaContract.Presenter {

    TriviaContract.View view;

    public TriviaPresenter(TriviaContract.View view) {
        this.view = view;
    }

    @Override
    public void generateTrivia(int pieces) {
        int length = Constant.TRIVIA.length - 1;
        Random rand = new Random();
        int rc = rand.nextInt(length);

        TriviaModel triviaModel = new TriviaModel();
        triviaModel.setSrc(Constant.TRIVIA[rc]);
        view.showTrivia(triviaModel);
    }
}
